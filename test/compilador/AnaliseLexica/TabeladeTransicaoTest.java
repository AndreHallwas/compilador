/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseLexica;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Raizen
 */
public class TabeladeTransicaoTest {
    
    public TabeladeTransicaoTest() {
    }
    
    @Test
    public void TesteGeral(){
        
        TabeladeTransicao tb = new TabeladeTransicao(3, 2, 0) {};
        tb.addEstadoFinal(2);
        tb.addSimbulo('a');
        tb.addSimbulo('b');
        tb.addTransicao(0, 'a', 1);
        tb.addTransicao(0, 'b', 0);
        tb.addTransicao(1, 'b', 2);
        
        int flag;
        
        flag = tb.ProximoSimbulo('b');
        flag = tb.ProximoSimbulo('a');
        System.out.println(flag);
        flag = tb.ProximoSimbulo('b');
        
        System.out.println(flag);
        System.out.println(tb.getCaracteres());
        assertEquals(flag, 2);
        
    }
    
}
