/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseLexica;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Raizen
 */
public class AutomatoLexicoTest {

    public AutomatoLexicoTest() {
    }

    /**
     * Test of add method, of class AutomatoLexico.
     */
    @Test
    public void testAdd() {
        AutomatoLexico atl = new AutomatoLexico();
        String teste = "int240;";
        int Valor = 0,Resultado = 2;
        for (int i = 0; i < teste.length() && Valor != 1 && Valor != -1; i++) {
            System.out.println(teste.charAt(i)+"");
            Valor = atl.ProximoSimbulo(teste.charAt(i));
            if(Valor == Resultado){
                System.out.println("Aprovado");
            }else if(Valor == 1){
                System.out.println("Rejeitado");
            }else if(Valor == 3){
                System.out.println("Aprovado, Retorna Caracter !!!!!");
                atl = new AutomatoLexico();
                i--;
            }
        }
        if(Valor == 0){
            atl.ProximoFinal();
        }
        assertEquals(Valor, Resultado);
    }

}
