/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.CodigodeMaquina;

import compilador.AnaliseLexica.NodeTransicao;
import compilador.CodigoIntermediario.Bloco;
import compilador.CodigoIntermediario.CodigoIntermediario;
import compilador.CodigoIntermediario.FirstBloco;
import compilador.CodigoIntermediario.TabelaId;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class CodigodeMaquina extends CodigoIntermediario{
    @Override
    public void gera(ArrayList<NodeTransicao> Tokens) {
        TabelaId.Clear();
        FirstBloco.Clear();
        Bloco.Inicializa();
        monta(Tokens);
        exibeComplexo();
    }
}
