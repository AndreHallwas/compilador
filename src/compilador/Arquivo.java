/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Aluno
 */
public class Arquivo {

    private File Arquivo = null;
    FileReader ler;
    Scanner reader;

    public Arquivo(String Caminho) {
        Inicialize(Caminho);
    }

    public void Inicialize(String Caminho) {
        Arquivo = new File(Caminho);
    }

    public void InicializaLeitura() {
        try {
            ler = new FileReader(Arquivo);
            reader = new Scanner(ler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String lerLinha() {
        String linha = "";
        if (Arquivo != null) {
            if (ler == null || reader == null) {
                InicializaLeitura();
            }
            if (reader.hasNext()) {
                linha = reader.nextLine();
            }else{
                linha = null;
            }
        }
        return linha;
    }

    public static String ler(String Caminho) {
        String Texto = "";
        try {
            FileReader ler = new FileReader(Caminho);
            BufferedReader reader = new BufferedReader(ler);
            String linha;
            while ((linha = reader.readLine()) != null) {
                Texto += linha;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Texto;
    }

    public static void gravar(String Caminho, String content) {
        try {
            File file = new File(Caminho);

            // Se o arquivo nao existir, ele gera
            if (!file.exists()) {
                file.createNewFile();
            }

            // Prepara para escrever no arquivo
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);

            // Escreve e fecha arquivo
            bw.write(content);
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
