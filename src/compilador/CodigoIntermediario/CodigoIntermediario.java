/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.CodigoIntermediario;

import compilador.AnaliseLexica.NodeTransicao;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Stack;

/**
 *
 * @author Raizen
 */
public class CodigoIntermediario {

    public void gera(ArrayList<NodeTransicao> Tokens) {
        TabelaId.Clear();
        FirstBloco.Clear();
        Bloco.Inicializa();
        Tokens = otimizaVariaveis(Tokens);
        Tokens = otimizaBlocoVazio(Tokens);
        monta(Tokens);        
        exibe();
    }

    protected void monta(ArrayList<NodeTransicao> Tokens) {
        Atribuicao.start();
        Repeticao.start();
        Pergunta.start();
        SimpleParser par = new SimpleParser(Tokens);
        NodeTransicao Token;
        while (par.isNotFinal()) {
            Token = par.prox();
            if (Token.getToken().equalsIgnoreCase("tk_id")) {
                Atribuicao.Add(Token, par);
            } else if (Token.getToken().equalsIgnoreCase("tk_enquanto")) {
                Repeticao.Add(Token, par);
            } else if (Token.getToken().equalsIgnoreCase("tk_pergunta")) {
                Pergunta.Add(Token, par);
            }
        }
        /*Repeticao.repeticoes;
        Atribuicao.atribuicoes;
        Pergunta.perguntas;*/
    }

    protected void exibe() {
        Stack pilha = new Stack();
        ArrayList<Atribuicao> atribuicoes = Atribuicao.getAtribuicoes();
        ArrayList<Repeticao> repeticoes = Repeticao.getRepeticoes();
        ArrayList<Pergunta> perguntas = Pergunta.getPerguntas();

        ArrayList<Object> Objetos = new ArrayList();
        Objetos.addAll(atribuicoes);
        Objetos.addAll(repeticoes);
        Objetos.addAll(perguntas);
        ArrayList<Object> ObjetosOrdenados = new ArrayList();
        while (!Objetos.isEmpty()) {
            Object ObjetoMinimo = Objetos.get(0);
            int SequenciaMinima = Integer.MAX_VALUE, SequenciaAtual = Integer.MAX_VALUE;
            if (ObjetoMinimo instanceof Pergunta) {
                SequenciaMinima = ((Pergunta) ObjetoMinimo).getSequencia();
            } else if (ObjetoMinimo instanceof Repeticao) {
                SequenciaMinima = ((Repeticao) ObjetoMinimo).getSequencia();
            } else if (ObjetoMinimo instanceof Atribuicao) {
                SequenciaMinima = ((Atribuicao) ObjetoMinimo).getSequencia();
            }
            for (int i = 1; i < Objetos.size(); i++) {
                Object object = Objetos.get(i);
                if (object instanceof Pergunta) {
                    SequenciaAtual = ((Pergunta) object).getSequencia();
                } else if (object instanceof Repeticao) {
                    SequenciaAtual = ((Repeticao) object).getSequencia();
                } else if (object instanceof Atribuicao) {
                    SequenciaAtual = ((Atribuicao) object).getSequencia();
                }
                if (SequenciaAtual < SequenciaMinima) {
                    ObjetoMinimo = object;
                    SequenciaMinima = SequenciaAtual;
                }
            }
            ObjetosOrdenados.add(ObjetoMinimo);
            Objetos.remove(ObjetoMinimo);
        }
        ArrayList<Object> OrdemReal = new ArrayList();
        for (Object ObjetoOrdenado : ObjetosOrdenados) {
            if (ObjetoOrdenado instanceof Pergunta) {
                ///OrdemReal.add(ObjetoOrdenado);
                pilha.push(ObjetoOrdenado);
            } else if (ObjetoOrdenado instanceof Repeticao) {
                ////OrdemReal.add(ObjetoOrdenado);
                pilha.push(ObjetoOrdenado);
            } else if (ObjetoOrdenado instanceof Atribuicao) {
                int blocoAtual = 0;
                if (!pilha.isEmpty() && pilha.peek() instanceof Repeticao
                        && ((Repeticao) pilha.peek()).getCommandBlock() < blocoAtual) {
                    OrdemReal.add(pilha.pop());
                } else if (!pilha.isEmpty() && pilha.peek() instanceof Pergunta
                        && ((Pergunta) pilha.peek()).getCommandBlock() < blocoAtual) {
                    OrdemReal.add(pilha.pop());
                }
                OrdemReal.add(ObjetoOrdenado);
            }
        }

        while (!pilha.isEmpty()) {
            OrdemReal.add(pilha.pop());
        }

        /*for (Object ObjetoOrdenado : OrdemReal) {
            if (ObjetoOrdenado instanceof Pergunta) {
                System.out.println(((Pergunta) ObjetoOrdenado).getSimpleCode());
            } else if (ObjetoOrdenado instanceof Repeticao) {
                System.out.println(((Repeticao) ObjetoOrdenado).getSimpleCode());
            } else if (ObjetoOrdenado instanceof Atribuicao) {
                System.out.println(((Atribuicao) ObjetoOrdenado).getSimpleCode());
            }
        }*/
        GerarArquivoSimple(OrdemReal);

    }

    protected void exibeComplexo() {
        Stack pilha = new Stack();
        ArrayList<Atribuicao> atribuicoes = Atribuicao.getAtribuicoes();
        ArrayList<Repeticao> repeticoes = Repeticao.getRepeticoes();
        ArrayList<Pergunta> perguntas = Pergunta.getPerguntas();

        ArrayList<Object> Objetos = new ArrayList();
        Objetos.addAll(atribuicoes);
        Objetos.addAll(repeticoes);
        Objetos.addAll(perguntas);
        ArrayList<Object> ObjetosOrdenados = new ArrayList();
        while (!Objetos.isEmpty()) {
            Object ObjetoMinimo = Objetos.get(0);
            int SequenciaMinima = Integer.MAX_VALUE, SequenciaAtual = Integer.MAX_VALUE;
            if (ObjetoMinimo instanceof Pergunta) {
                SequenciaMinima = ((Pergunta) ObjetoMinimo).getSequencia();
            } else if (ObjetoMinimo instanceof Repeticao) {
                SequenciaMinima = ((Repeticao) ObjetoMinimo).getSequencia();
            } else if (ObjetoMinimo instanceof Atribuicao) {
                SequenciaMinima = ((Atribuicao) ObjetoMinimo).getSequencia();
            }
            for (int i = 1; i < Objetos.size(); i++) {
                Object object = Objetos.get(i);
                if (object instanceof Pergunta) {
                    SequenciaAtual = ((Pergunta) object).getSequencia();
                } else if (object instanceof Repeticao) {
                    SequenciaAtual = ((Repeticao) object).getSequencia();
                } else if (object instanceof Atribuicao) {
                    SequenciaAtual = ((Atribuicao) object).getSequencia();
                }
                if (SequenciaAtual < SequenciaMinima) {
                    ObjetoMinimo = object;
                    SequenciaMinima = SequenciaAtual;
                }
            }
            ObjetosOrdenados.add(ObjetoMinimo);
            Objetos.remove(ObjetoMinimo);
        }
        ArrayList<Object> OrdemReal = new ArrayList();
        for (Object ObjetoOrdenado : ObjetosOrdenados) {
            if (ObjetoOrdenado instanceof Pergunta) {
                ///OrdemReal.add(ObjetoOrdenado);
                pilha.push(ObjetoOrdenado);
            } else if (ObjetoOrdenado instanceof Repeticao) {
                ////OrdemReal.add(ObjetoOrdenado);
                pilha.push(ObjetoOrdenado);
            } else if (ObjetoOrdenado instanceof Atribuicao) {
                int blocoAtual = 0;
                if (!pilha.isEmpty() && pilha.peek() instanceof Repeticao
                        && ((Repeticao) pilha.peek()).getCommandBlock() < blocoAtual) {
                    OrdemReal.add(pilha.pop());
                } else if (!pilha.isEmpty() && pilha.peek() instanceof Pergunta
                        && ((Pergunta) pilha.peek()).getCommandBlock() < blocoAtual) {
                    OrdemReal.add(pilha.pop());
                }
                OrdemReal.add(ObjetoOrdenado);
            }
        }

        while (!pilha.isEmpty()) {
            OrdemReal.add(pilha.pop());
        }

        OrdemReal.add(new Halt());
        /*
        for (Object ObjetoOrdenado : OrdemReal) {
            if (ObjetoOrdenado instanceof Pergunta) {
                System.out.println(((Pergunta) ObjetoOrdenado).getComplexCode());
            } else if (ObjetoOrdenado instanceof Repeticao) {
                System.out.println(((Repeticao) ObjetoOrdenado).getComplexCode());
            } else if (ObjetoOrdenado instanceof Atribuicao) {
                System.out.println(((Atribuicao) ObjetoOrdenado).getComplexCode());
            } else if (ObjetoOrdenado instanceof Halt) {
                System.out.println(((Halt) ObjetoOrdenado).getHalt());
            }
        }*/

        GerarArquivoComplex(OrdemReal);

    }

    protected void GerarArquivoSimple(ArrayList<Object> OrdemReal) {
        FileWriter arq;
        try {
            arq = new FileWriter(new File("CodigoIntermediario.o"));
            PrintWriter gravarArq = new PrintWriter(arq);
            for (Object ObjetoOrdenado : OrdemReal) {
                if (ObjetoOrdenado instanceof Pergunta) {
                    gravarArq.printf("%s %n", ((Pergunta) ObjetoOrdenado).getSimpleCode());
                } else if (ObjetoOrdenado instanceof Repeticao) {
                    gravarArq.printf("%s %n", ((Repeticao) ObjetoOrdenado).getSimpleCode());
                } else if (ObjetoOrdenado instanceof Atribuicao) {
                    gravarArq.printf("%s %n", ((Atribuicao) ObjetoOrdenado).getSimpleCode());
                }
            }
            arq.close();
        } catch (Exception ex) {
            System.out.println("Erro: " + ex);
        }

    }

    protected void GerarArquivoComplex(ArrayList<Object> OrdemReal) {
        FileWriter arq;
        try {
            arq = new FileWriter(new File("CodigodeMaquina.asm"));
            PrintWriter gravarArq = new PrintWriter(arq);
            for (Object ObjetoOrdenado : OrdemReal) {
                if (ObjetoOrdenado instanceof Pergunta) {
                    gravarArq.printf("%s %n", ((Pergunta) ObjetoOrdenado).getComplexCode());
                } else if (ObjetoOrdenado instanceof Repeticao) {
                    gravarArq.printf("%s %n", ((Repeticao) ObjetoOrdenado).getComplexCode());
                } else if (ObjetoOrdenado instanceof Atribuicao) {
                    gravarArq.printf("%s %n", ((Atribuicao) ObjetoOrdenado).getComplexCode());
                } else if (ObjetoOrdenado instanceof Halt) {
                    gravarArq.printf("%s %n", ((Halt) ObjetoOrdenado).getHalt());
                }
            }
            arq.close();
        } catch (Exception ex) {
            System.out.println("Erro: " + ex);
        }

    }

    private ArrayList<NodeTransicao> otimizaVariaveis(ArrayList<NodeTransicao> Tokens) {
        ArrayList<NodeTransicao> oldTokens = new ArrayList(Tokens);
        TabelaId.Clear();
        for (NodeTransicao Token : Tokens) {
            if (Token.getToken().equalsIgnoreCase("tk_id")) {
                TabelaId.getRegistrador(Token.getCaracteres());
            }
        }
        for (String string : TabelaId.getUsos(1)) {
            int i = oldTokens.size()-1;
            while (i > -1 && !oldTokens.get(i).getCaracteres().equalsIgnoreCase(string)) {
                i--;
            }
            if(i > -1){
               oldTokens.remove(i);
            }
        }
        return oldTokens;
    }

    private ArrayList<NodeTransicao> otimizaBlocoVazio(ArrayList<NodeTransicao> Tokens) {
        /*ArrayList<NodeTransicao> oldTokens = new ArrayList(Tokens);
        for (NodeTransicao oldToken : oldTokens) {
            for (String string : TabelaId.getUsos(1)) {
            int i = oldTokens.size()-1;
            while (i > -1 && !oldTokens.get(i).getCaracteres().equalsIgnoreCase(string)) {
                i--;
            }
            if(i > -1){
               oldTokens.remove(i);
            }
        }
        }*/
        return Tokens;
    }
}
