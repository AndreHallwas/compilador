/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.CodigoIntermediario;

import compilador.AnaliseLexica.NodeTransicao;
import static compilador.CodigoIntermediario.Atribuicao.atribuicoes;
import java.util.ArrayList;
import java.util.Stack;
import sun.misc.Queue;

/**
 *
 * @author Raizen
 */
public class Repeticao {

    protected static ArrayList<Repeticao> repeticoes = new ArrayList();
    protected Stack pi;
    protected int Sequencia;
    protected int bloco;
    protected int CommandBlock;

    public Repeticao(Stack pi) {
        this.pi = pi;
        this.Sequencia = Seq.next();
        this.bloco = Bloco.getCurrent();
        this.CommandBlock = Bloco.setNext();
    }

    public static void start() {
        setRepeticoes((ArrayList<Repeticao>) new ArrayList());
    }

    public static void Add(NodeTransicao Token, SimpleParser par) {
        Stack pilha;
        pilha = new Stack();
        pilha.add(Token);
        Token = par.prox();
        if (Token != null && Token.getToken().equalsIgnoreCase("tk_abrir_parentese")) {
            Token = par.prox();
            if (Token != null && Token.getToken().equalsIgnoreCase("tk_caracter")
                    || Token.getToken().equalsIgnoreCase("tk_id")
                    || Token.getToken().equalsIgnoreCase("tk_numero")) {
                pilha.add(Token);
                Token = par.prox();
                if (Token != null && Token.getToken().equalsIgnoreCase("tk_igual")
                        || Token.getToken().equalsIgnoreCase("tk_maior_igual")
                        || Token.getToken().equalsIgnoreCase("tk_menor_igual")
                        || Token.getToken().equalsIgnoreCase("tk_diferente_igual")
                        || Token.getToken().equalsIgnoreCase("tk_menor")) {
                    pilha.add(Token);
                    Token = par.prox();
                    if (Token != null && Token.getToken().equalsIgnoreCase("tk_numero")
                            || Token.getToken().equalsIgnoreCase("tk_id")) {
                        pilha.add(Token);
                        Token = par.prox();
                        if (Token != null && Token.getToken().equalsIgnoreCase("tk_fechar_parentese")) {
                            Token = par.prox();
                            if (Token != null && Token.getToken().equalsIgnoreCase("tk_abrir_chave")) {
                                getRepeticoes().add(new Repeticao(pilha));
                            }
                        }
                    }
                }
            }
        }
    }

    public String getSimpleCode() {
        Stack aux = new Stack();
        NodeTransicao auxNode;
        while (!pi.isEmpty()) {
            auxNode = ((NodeTransicao) pi.pop());
            if (auxNode.getToken().equalsIgnoreCase("tk_enquanto")) {
                aux.push("goto");
            } else {
                aux.push(auxNode.getCaracteres());
            }
        }
        String Line = "Bloco " + bloco + ": ";

        while (!aux.isEmpty()) {
            Line += (String) aux.pop() + " ";
        }
        Line += "," + CommandBlock + " ";

        return Line;
    }

    public String getComplexCode() {
        Stack aux = new Stack();
        Queue lista = new Queue();
        NodeTransicao auxNode;
        boolean igual = false;
        boolean menorigual = false;
        while (!pi.isEmpty()) {
            auxNode = ((NodeTransicao) pi.pop());
            if (auxNode.getToken().equalsIgnoreCase("tk_id")) {
                lista.enqueue(appendLoadR0(auxNode));
                aux.push("R0");
                if (!pi.isEmpty()) {
                    auxNode = ((NodeTransicao) pi.pop());
                    if (auxNode.getToken().equalsIgnoreCase("tk_igual")) {
                        aux.push("=");
                        if (!pi.isEmpty()) {
                            auxNode = ((NodeTransicao) pi.pop());
                            if (auxNode.getToken().equalsIgnoreCase("tk_id")) {
                                aux.push(TabelaId.getRegistrador(auxNode.getCaracteres()));
                                if (!pi.isEmpty()) {
                                    auxNode = ((NodeTransicao) pi.pop());
                                    if (auxNode.getToken().equalsIgnoreCase("tk_enquanto")) {
                                        aux.push("jmpEQ");
                                    }
                                }
                            }
                        }
                    } else if (auxNode.getToken().equalsIgnoreCase("tk_menor_igual")) {
                        aux.push("<=");
                        if (!pi.isEmpty()) {
                            auxNode = ((NodeTransicao) pi.pop());
                            if (auxNode.getToken().equalsIgnoreCase("tk_id")) {
                                aux.push(TabelaId.getRegistrador(auxNode.getCaracteres()));
                                if (!pi.isEmpty()) {
                                    auxNode = ((NodeTransicao) pi.pop());
                                    if (auxNode.getToken().equalsIgnoreCase("tk_enquanto")) {
                                        aux.push("jmpLE");
                                    }
                                }
                            }
                        }
                    }
                } else {
                    aux.push(auxNode.getCaracteres());
                }
            }
        }
        lista.enqueue(aux);

        String Line;
        if (FirstBloco.isFirst(bloco + "")) {
            Line = "Bloco" + bloco + ": ";
        } else {
            Line = "";
        }
        
        lista.enqueue(aux);
        try {
            aux = (Stack) lista.dequeue();
        } catch (InterruptedException ex) {
            System.out.println("Erro:" + ex);
        }

        Line += (String) aux.pop() + " ";
        while (!aux.isEmpty()) {
            Line += (String) aux.pop() + " ";
        }
        Line += "\n";
        try {
            aux = (Stack) lista.dequeue();
        } catch (InterruptedException ex) {
            System.out.println("Erro:" + ex);
        }

        while (!aux.isEmpty()) {
            Line += " " + (String) aux.pop();
        }
        Line += ", Bloco" + CommandBlock + " ";

        return Line;
    }

    /**
     * @return the repeticoes
     */
    public static ArrayList<Repeticao> getRepeticoes() {
        return repeticoes;
    }

    /**
     * @param aRepeticoes the repeticoes to set
     */
    public static void setRepeticoes(ArrayList<Repeticao> aRepeticoes) {
        repeticoes = aRepeticoes;
    }

    /**
     * @return the pi
     */
    public Stack getPi() {
        return pi;
    }

    /**
     * @param pi the pi to set
     */
    public void setPi(Stack pi) {
        this.pi = pi;
    }

    /**
     * @return the Sequencia
     */
    public int getSequencia() {
        return Sequencia;
    }

    /**
     * @param Sequencia the Sequencia to set
     */
    public void setSequencia(int Sequencia) {
        this.Sequencia = Sequencia;
    }

    /**
     * @return the bloco
     */
    public int getBloco() {
        return bloco;
    }

    /**
     * @param bloco the bloco to set
     */
    public void setBloco(int bloco) {
        this.bloco = bloco;
    }

    /**
     * @return the CommandBlock
     */
    public int getCommandBlock() {
        return CommandBlock;
    }

    /**
     * @param CommandBlock the CommandBlock to set
     */
    public void setCommandBlock(int CommandBlock) {
        this.CommandBlock = CommandBlock;
    }

    private Object appendLoadR0(NodeTransicao auxNode) {
        Stack auxIns = new Stack();
        String Registrador = "R0";
        auxIns.push(TabelaId.getRegistrador(auxNode.getCaracteres()));
        auxIns.push(",");
        TabelaId.addUso(Registrador,2);
        auxIns.push(Registrador);
        auxIns.push("move");
        return auxIns;
    }
}
