/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.CodigoIntermediario;

/**
 *
 * @author Raizen
 */
public class Seq {

    protected static int num;

    public void Inicializa() {
        setNum(0);
    }

    public static int next() {
        return ++num;
    }

    /**
     * @return the num
     */
    public static int getNum() {
        return num;
    }

    /**
     * @param aNum the num to set
     */
    public static void setNum(int aNum) {
        num = aNum;
    }
}
