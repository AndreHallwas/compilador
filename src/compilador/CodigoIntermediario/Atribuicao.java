/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.CodigoIntermediario;

import compilador.AnaliseLexica.NodeTransicao;
import java.util.ArrayList;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.Queue;

/**
 *
 * @author Raizen
 */
public class Atribuicao {

    protected static ArrayList<Atribuicao> atribuicoes = new ArrayList();
    protected Stack pi;
    protected int Sequencia;
    protected int bloco;

    public Atribuicao(Stack pi) {
        this.pi = pi;
        this.Sequencia = Seq.next();
        this.bloco = Bloco.getCurrent();
    }

    public static void start() {
        setAtribuicoes((ArrayList<Atribuicao>) new ArrayList());
    }

    public static void Add(NodeTransicao Token, SimpleParser par) {
        Stack pilha;
        pilha = new Stack();
        pilha.add(Token);
        Token = par.prox();
        if (Token != null && Token.getToken().equalsIgnoreCase("tk_atribuicao")) {
            Token = par.prox();
            if (Token != null) {
                if (Token.getToken().equalsIgnoreCase("tk_caracter")
                        || Token.getToken().equalsIgnoreCase("tk_id")
                        || Token.getToken().equalsIgnoreCase("tk_numero")) {
                    pilha.add(Token);
                } else if (Token.getToken().equalsIgnoreCase("tk_abrir_parentese")) {
                    /////pilha.add(Token);
                    Token = par.prox();
                    if (Token != null && Token.getToken().equalsIgnoreCase("tk_numero")
                            || Token.getToken().equalsIgnoreCase("tk_id")) {
                        pilha.add(Token);
                        Token = par.prox();
                        if (Token != null && Token.getToken().equalsIgnoreCase("tk_adicao")
                                || Token.getToken().equalsIgnoreCase("tk_subtracao")
                                || Token.getToken().equalsIgnoreCase("tk_divisao")
                                || Token.getToken().equalsIgnoreCase("tk_multiplicacao")) {
                            pilha.add(Token);
                            Token = par.prox();
                            if (Token != null && Token.getToken().equalsIgnoreCase("tk_numero")
                                    || Token.getToken().equalsIgnoreCase("tk_id")) {
                                pilha.add(Token);
                            }
                        }
                    }
                }
                getAtribuicoes().add(new Atribuicao(pilha));
            }
        }
    }

    public String getSimpleCode() {
        Stack aux = new Stack();
        NodeTransicao auxNode;
        while (!pi.isEmpty()) {
            auxNode = ((NodeTransicao) pi.pop());
            aux.push(auxNode.getCaracteres());
        }
        String Line = "";

        Line += "Bloco " + bloco + ": " + (String) aux.pop() + " := ";
        while (!aux.isEmpty()) {
            Line += (String) aux.pop() + " ";
        }

        return Line;
    }

    public String getComplexCode() {
        Stack aux = new Stack();
        Stack auxIns;
        Queue lista = new Queue();
        NodeTransicao auxNode;
        while (!pi.isEmpty()) {
            auxNode = ((NodeTransicao) pi.pop());
            if (auxNode.getToken().equalsIgnoreCase("tk_id")) {
                NodeTransicao auxNovo;
                aux.push(TabelaId.getRegistrador(auxNode.getCaracteres()));
                if (!pi.isEmpty()) {
                    auxNovo = ((NodeTransicao) pi.pop());
                    if (auxNovo.getToken().equalsIgnoreCase("tk_id")) {
                        aux.push(TabelaId.getRegistrador(auxNovo.getCaracteres()) + ",");
                        aux.push("move ");
                    } else if (auxNovo.getToken().equalsIgnoreCase("tk_adicao")
                            || auxNovo.getToken().equalsIgnoreCase("tk_subtracao")) {
                         String Tipo = auxNode.getToken();
                        if (!pi.isEmpty()) {
                            auxNode = ((NodeTransicao) pi.pop());
                            if (auxNode.getToken().equalsIgnoreCase("tk_numero")) {
                                if (auxNovo.getToken().equalsIgnoreCase("tk_subtracao")) {
                                    auxNode.setCaracteres("-" + auxNode.getCaracteres());
                                }
                                auxIns = (Stack) appendLoad(auxNode);
                                lista.enqueue(auxIns);
                                aux.push(auxIns.pop() + ",");
                                auxNovo = auxNode;
                                if (!pi.isEmpty()) {
                                    auxNode = ((NodeTransicao) pi.pop());
                                    if (auxNode.getToken().equalsIgnoreCase("tk_id")) {
                                        aux.push(TabelaId.getRegistrador(auxNode.getCaracteres()) + ",");
                                        aux.push("addi");
                                    }
                                }
                            } else if (auxNode.getToken().equalsIgnoreCase("tk_id")) {
                                if (auxNode.getToken().equalsIgnoreCase("tk_subtracao")) {
                                    auxNode.setCaracteres("-" + auxNode.getCaracteres());
                                }
                                aux.push(TabelaId.getRegistrador(auxNode.getCaracteres()) + ",");
                                if (!pi.isEmpty()) {
                                    auxNode = ((NodeTransicao) pi.pop());
                                    if (auxNode.getToken().equalsIgnoreCase("tk_id")) {
                                        aux.push(TabelaId.getRegistrador(auxNode.getCaracteres()) + ",");
                                        aux.push("addi");
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (auxNode.getToken().equalsIgnoreCase("tk_numero")) {
                NodeTransicao auxNovo;
                if (!pi.isEmpty()) {
                    auxNovo = ((NodeTransicao) pi.pop());
                    if (auxNovo.getToken().equalsIgnoreCase("tk_adicao")
                            || auxNovo.getToken().equalsIgnoreCase("tk_subtracao")) {
                        if (auxNovo.getToken().equalsIgnoreCase("tk_subtracao")) {
                            auxNode.setCaracteres("-" + auxNode.getCaracteres());
                        }
                        auxIns = (Stack) appendLoad(auxNode);
                        lista.enqueue(auxIns);
                        aux.push(auxIns.pop());
                        auxNode = auxNovo;
                        if (!pi.isEmpty()) {
                            auxNode = ((NodeTransicao) pi.pop());
                            if (auxNode.getToken().equalsIgnoreCase("tk_numero")) {
                                auxIns = (Stack) appendLoad(auxNode);
                                lista.enqueue(auxIns);
                                aux.push(auxIns.pop() + ",");
                                if (!pi.isEmpty()) {
                                    auxNode = ((NodeTransicao) pi.pop());
                                    if (auxNode.getToken().equalsIgnoreCase("tk_id")) {
                                        aux.push(TabelaId.getRegistrador(auxNode.getCaracteres()) + ",");
                                        aux.push("addi");
                                    }
                                }
                            } else if (auxNode.getToken().equalsIgnoreCase("tk_id")) {
                                aux.push(TabelaId.getRegistrador(auxNode.getCaracteres()) + ",");
                                if (!pi.isEmpty()) {
                                    auxNode = ((NodeTransicao) pi.pop());
                                    if (auxNode.getToken().equalsIgnoreCase("tk_id")) {
                                        aux.push(TabelaId.getRegistrador(auxNode.getCaracteres()) + ",");
                                        aux.push("addi");
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                aux.push(auxNode.getCaracteres());
            }
        }
        String Line;
        if (FirstBloco.isFirst(bloco + "")) {
            Line = "Bloco" + bloco + ": ";
        } else {
            Line = "";
        }
        lista.enqueue(aux);
        while (!lista.isEmpty()) {
            try {
                aux = (Stack) lista.dequeue();
            } catch (InterruptedException ex) {
                System.out.println("Erro:" + ex);
            }

            Line += (String) aux.pop() + " ";
            while (!aux.isEmpty()) {
                Line += (String) aux.pop() + " ";
            }
            if (!lista.isEmpty()) {
                Line += "\n";
            }
        }

        return Line;
    }

    /**
     * @return the atribuicoes
     */
    public static ArrayList<Atribuicao> getAtribuicoes() {
        return atribuicoes;
    }

    /**
     * @param aAtribuicoes the atribuicoes to set
     */
    public static void setAtribuicoes(ArrayList<Atribuicao> aAtribuicoes) {
        atribuicoes = aAtribuicoes;
    }

    /**
     * @return the pi
     */
    public Stack getPi() {
        return pi;
    }

    /**
     * @param pi the pi to set
     */
    public void setPi(Stack pi) {
        this.pi = pi;
    }

    /**
     * @return the Sequencia
     */
    public int getSequencia() {
        return Sequencia;
    }

    /**
     * @param Sequencia the Sequencia to set
     */
    public void setSequencia(int Sequencia) {
        this.Sequencia = Sequencia;
    }

    /**
     * @return the bloco
     */
    public int getBloco() {
        return bloco;
    }

    /**
     * @param bloco the bloco to set
     */
    public void setBloco(int bloco) {
        this.bloco = bloco;
    }

    private Object appendLoad(NodeTransicao auxNode) {
        Stack auxIns = new Stack();
        String Registrador = TabelaId.getNextRegistrador();
        auxIns.push(auxNode.getCaracteres());
        auxIns.push(",");
        auxIns.push(Registrador);
        TabelaId.addUso(Registrador,1);
        auxIns.push("load");
        auxIns.push(Registrador);
        return auxIns;
    }
}
