/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.CodigoIntermediario;

/**
 *
 * @author Raizen
 */
public class Bloco {

    private static int Bloco;

    public static void Inicializa() {
        Bloco = 0;
    }

    public static int getCurrent() {
        return Bloco;
    }

    public static int setNext() {
        return ++Bloco;
    }
}
