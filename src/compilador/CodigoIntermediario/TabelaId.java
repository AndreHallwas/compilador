/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.CodigoIntermediario;

import compilador.AnaliseLexica.NodeTransicao;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class TabelaId {

    private static ArrayList<String> Id;
    private static ArrayList<Integer> Usos;
    private static ArrayList<String> Registrador;
    private static int RegistradorSeq;

    private static int buscaId(String id) {
        int i = 0;
        while (i < Id.size() && !Id.get(i).equalsIgnoreCase(id)) {
            i++;
        }
        return i < Id.size() ? i : -1;
    }

    private static int buscaRegistrador(String id) {
        int i = 0;
        while (i < Registrador.size() && !Registrador.get(i).equalsIgnoreCase(id)) {
            i++;
        }
        return i < Registrador.size() ? i : -1;
    }

    public static String getRegistrador(String id) {
        if (Id == null || Registrador == null) {
            Clear();
        }
        int Pos = buscaId(id);
        String Reg;
        if (Pos == -1) {
            Id.add(id);
            Reg = "R" + RegistradorSeq++;
            Registrador.add(Reg);
            Usos.add(1);
        } else {
            Reg = Registrador.get(Pos);
            Usos.set(Pos, Usos.get(Pos)+1);
        }
        return Reg;
    }
    
    public static String getNextRegistrador() {
        if (Id == null || Registrador == null) {
            Clear();
        }
        String Reg;
        Reg = "R" + RegistradorSeq++;
        Id.add(Reg);
        Registrador.add(Reg);
        Usos.add(1);
        return Reg;
    }

    public static void Clear() {
        Id = new ArrayList();
        Registrador = new ArrayList();
        Usos = new ArrayList();
        RegistradorSeq = 1;
    }

    public static void addUso(String registrador, int i) {
        if (Id == null || Registrador == null) {
            Clear();
        }
        int Pos = buscaRegistrador(registrador);
        String Reg;
        if (Pos != -1) {
            Usos.set(Pos, Usos.get(Pos)+i);
        }else{
            Id.add(registrador);
            Reg = "R" + RegistradorSeq++;
            Registrador.add(Reg);
            Usos.add(1);
        }
    }

    public static ArrayList<String> getUsos(int k) {
        ArrayList<String> Retorno = new ArrayList();
        for (int i = 0; i < Usos.size(); i++) {
            Integer Uso = Usos.get(i);
            if(Uso <= k){
                Retorno.add(Id.get(i));
            }
        }
        return Retorno;
    }
}
