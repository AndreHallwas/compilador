/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.CodigoIntermediario;

import compilador.AnaliseLexica.NodeTransicao;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class SimpleParser {
    private ArrayList<NodeTransicao> Tokens;
    int Pos = 0;

    public SimpleParser(ArrayList<NodeTransicao> Tokens) {
        this.Tokens = new ArrayList();
        this.Tokens.addAll(Tokens);
    }

    public NodeTransicao prox() {
        if (Pos < Tokens.size()) {
            return Tokens.get(Pos++);
        }
        return null;
    }
    public boolean isNotFinal(){
        return Pos < Tokens.size();
    }
}
