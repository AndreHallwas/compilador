/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.CodigoIntermediario;

import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class FirstBloco {

    private static ArrayList<String> blocos;

    public static boolean isFirst(String Bloco) {
        if (blocos == null) {
            Clear();
        }
        int Pos = BuscaBloco(Bloco);
        if (Pos == -1) {
            blocos.add(Bloco);
            return true;
        }
        return false;
    }

    private static int BuscaBloco(String Bloco) {
        int i = 0;
        while (i < blocos.size() && !blocos.get(i).equalsIgnoreCase(Bloco)) {
            i++;
        }
        return i < blocos.size() ? i : -1;
    }

    public static void Clear() {
        blocos = new ArrayList();
    }
}
