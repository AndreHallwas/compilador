/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseSintatica;

/**
 *
 * @author Raizen
 */
public class AnaliseSintatica extends TabelaSintatica {

    public AnaliseSintatica() {
        super(0, 25, 36 /*33*/);/*final -> Simbulo Final, ' ' ->Separador de Tokens*/
        addEstados("<programa>", "<operador>", "<operador_logico>", "<operador_atribuicao>", "<operador_matematico>");
        addEstados("<pergunta>","<pergunta_else>", "<repeticao>");
        addEstados("<termo>", "<termo_simples>");
        addEstados("<expressao>", "<expressao_logica>", "<expressao_matematica>",
                "<expressao_atribuicao>", "<expressao_simples_exp>", "<expressao_simples>");
        addEstados("<variavel>", "<variavel_exp>", "<tipo_de_variavel>");
        addEstados("<bloco>", "<inicio>", "<conteudo>");
        addToken("tk_abrir_parentese", "tk_fechar_parentese", "tk_aspas", "tk_abrir_chave", "tk_fechar_chave",
                "tk_abrir_colchete", "tk_fechar_colchete", "tk_ponto_virgula");
        addToken("tk_virgula", "tk_comentario");
        addToken("tk_adicao", "tk_subtracao", "tk_multiplicacao", "tk_divisao", "tk_resto_divisao");
        addToken("tk_maior", "tk_menor", "tk_maior_igual", "tk_menor_igual", "tk_igual",
                "tk_negacao", "tk_diferente_igual");
        addToken("tk_enquanto", "tk_else", "tk_pergunta");
        addToken("tk_atribuicao", "tk_incrementa_atribuicao", "tk_decrementa_atribuicao");
        addToken("tk_inteiro", "tk_cientifico", "tk_letra");
        addToken("tk_numero", "tk_caracter", "tk_id");
        addToken("tk_main", "tk_vazio");

        addRegraFinal("<operador>", "tk_maior", "tk_menor", "tk_maior_igual",
                "tk_menor_igual", "tk_igual", "tk_negacao", "tk_diferente_igual",
                "tk_atribuicao", "tk_incrementa_atribuicao", "tk_decrementa_atribuicao",
                "tk_adicao", "tk_subtracao", "tk_multiplicacao", "tk_divisao", "tk_resto_divisao");///x
        addRegraFinal("<operador_logico>", "tk_maior", "tk_menor", "tk_maior_igual",
                "tk_menor_igual", "tk_igual", "tk_negacao", "tk_diferente_igual");///x
        addRegraFinal("<operador_atribuicao>", "tk_atribuicao", "tk_incrementa_atribuicao",
                "tk_decrementa_atribuicao");///x
        addRegraFinal("<operador_matematico>", "tk_adicao", "tk_subtracao",
                "tk_multiplicacao", "tk_divisao", "tk_resto_divisao");///x

        addRegra("<pergunta>", "tk_pergunta", "tk_pergunta tk_abrir_parentese <expressao> tk_fechar_parentese <bloco> <pergunta_else>");///x
        addRegra("<pergunta_else>", "tk_else", "tk_else <bloco>");///x
        addRegra("<pergunta_else>", "tk_vazio", "tk_vazio");///x
        
        addRegra("<repeticao>", "tk_enquanto", "tk_enquanto tk_abrir_parentese <expressao> tk_fechar_parentese <bloco>");///x

        addRegraFinal("<termo>", "tk_id", "tk_numero", "tk_caracter");///x

        addRegra("<termo_simples>", "<termo>", "tk_id", "tk_numero", "tk_caracter");///x
        addRegra("<termo_simples>", "tk_abrir_parentese", "tk_abrir_parentese <expressao_simples> tk_fechar_parentese");///x
        
        addRegra("<expressao>", "<expressao_logica>", "tk_id", "tk_numero", "tk_caracter", "tk_abrir_parentese");///x
        
        addRegra("<expressao_logica>", "<termo_simples> <operador_logico> <termo_simples>", "tk_id", "tk_numero", "tk_caracter", "tk_abrir_parentese");///x

        addRegra("<expressao_matematica>", "<termo_simples> <operador_matematico> <termo_simples>", "tk_id", "tk_numero", "tk_caracter", "tk_abrir_parentese");///x

        addRegra("<expressao_atribuicao>", "tk_id", "tk_id <operador_atribuicao> <termo_simples>");///x

        addRegra("<expressao_simples>", "<expressao_matematica>", "tk_numero", "tk_caracter", "tk_abrir_parentese");///x
        addRegra("<expressao_simples>", "tk_id", "tk_id <expressao_simples_exp>");///x
        addRegra("<expressao_simples_exp>", "<operador_atribuicao> <termo_simples>", "tk_atribuicao", "tk_incrementa_atribuicao",
                "tk_decrementa_atribuicao");///x
        addRegra("<expressao_simples_exp>", "<operador_matematico> <termo_simples>", "tk_adicao", "tk_subtracao",
                "tk_multiplicacao", "tk_divisao", "tk_resto_divisao");///x
        
        addRegra("<variavel>", "<tipo_de_variavel> tk_id <variavel_exp>", "tk_inteiro", "tk_cientifico", "tk_letra");///x
        addRegra("<variavel_exp>", "tk_atribuicao", "<operador_atribuicao> <termo_simples>");///x
        addRegra("<variavel_exp>", "tk_vazio", "tk_vazio");///x
        addRegraFinal("<tipo_de_variavel>", "tk_inteiro", "tk_cientifico", "tk_letra");///x


        addRegra("<bloco>", "tk_abrir_chave", "tk_abrir_chave <conteudo> tk_fechar_chave");///x

        addRegra("<programa>", "tk_abrir_chave", "<bloco>");///x

        addRegra("<inicio>", "tk_main", "tk_main");

        addRegra("<conteudo>", "<variavel> tk_ponto_virgula <conteudo>", "tk_inteiro", "tk_cientifico", "tk_letra");///x
        addRegra("<conteudo>", "tk_pergunta", "<pergunta> <conteudo>");///x
        addRegra("<conteudo>", "tk_enquanto", "<repeticao> <conteudo>");///x
        addRegra("<conteudo>", "tk_id", "<expressao_atribuicao> tk_ponto_virgula <conteudo>");///x
        addRegra("<conteudo>", "tk_abrir_chave", "<bloco> <conteudo>");
        addRegra("<conteudo>", "tk_vazio", "tk_vazio");
        /*addRegra("<conteudo>", "tk_comentario", "tk_comentario");*/
    }
}
