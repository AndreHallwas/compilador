/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseSintatica;

import java.util.ArrayList;
import java.util.Stack;

/**
 *
 * @author Raizen
 */
public abstract class TabelaSintatica {

    protected Stack<String> Pilha;
    protected ArrayList<String> Tokens;
    protected ArrayList<String> Estados;
    protected String[][] Regras;
    protected int EstadoInicial;
    protected int TamEstados;
    protected int TamTokens;
    private ArrayList<Integer> TokensComErro;

    public TabelaSintatica(int EstadoInicial, int TamEstados, int TamTokens) {
        Inicializa(TamEstados, TamTokens, EstadoInicial);
    }

    protected void Inicializa(int TamEstados, int TamTokens, int EstadoInicial) {
        Tokens = new ArrayList();
        Estados = new ArrayList();
        Pilha = new Stack();

        this.TamEstados = TamEstados;
        this.TamTokens = TamTokens;
        this.EstadoInicial = EstadoInicial;

        InicializaRegras(TamEstados, TamTokens);
    }

    protected void InicializaRegras(int Estados, int Tokens) {
        Regras = new String[Estados][];
        for (int i = 0; i < Regras.length; i++) {
            Regras[i] = new String[TamTokens];
            for (int j = 0; j < TamTokens; j++) {
                Regras[i][j] = "";
            }
        }
    }

    public void addToken(String Token) {
        if (Tokens != null && Tokens.size() <= TamTokens) {
            Tokens.add(Token);
        }
    }

    public void addToken(String... Tokens) {
        if (Tokens != null) {
            int Tam = Tokens.length;
            for (int i = 0; i < Tam; i++) {
                addToken(Tokens[i]);
            }
        }
    }

    public void addEstados(String Estado) {
        if (Estados != null && Estados.size() <= TamEstados) {
            Estados.add(Estado);
        }
    }

    public void addEstados(String... Estados) {
        if (Estados != null) {
            int Tam = Estados.length;
            for (int i = 0; i < Tam; i++) {
                addEstados(Estados[i]);
            }
        }
    }

    public void addRegra(String Estado, String Token, String Regra) {
        int PosEstado = BuscaEstado(Estado);
        if (PosEstado != -1) {
            int PosToken = BuscaToken(Token);
            if (PosToken != -1) {
                Regras[PosEstado][PosToken] = Regra;
            } else {
                System.out.println(Token + "=> Token não Cadastrado na Tabela!!!!!!");
            }
        } else {
            System.out.println(Estado + "=> Estado não Cadastrado na Tabela!!!!!!");
        }
    }

    public void addRegra(String Estado, String Regra, String... Tokens) {
        int PosEstado = BuscaEstado(Estado);
        if (PosEstado != -1) {
            if (Tokens != null) {
                int Tam = Tokens.length;
                for (int i = 0; i < Tam; i++) {
                    int PosToken = BuscaToken(Tokens[i]);
                    if (PosToken != -1) {
                        Regras[PosEstado][PosToken] = Regra;
                    } else {
                        System.out.println(Tokens[i] + "=> Token não Cadastrado na Tabela!!!!!!");
                    }
                }
            }
        } else {
            System.out.println(Estado + "=> Estado não Cadastrado na Tabela!!!!!!");
        }
    }

    public void addRegraFinal(String Estado, String... Tokens) {
        int PosEstado = BuscaEstado(Estado);
        if (PosEstado != -1) {
            if (Tokens != null) {
                int Tam = Tokens.length;
                for (int i = 0; i < Tam; i++) {
                    int PosToken = BuscaToken(Tokens[i]);
                    if (PosToken != -1) {
                        Regras[PosEstado][PosToken] = Tokens[i];
                    } else {
                        System.out.println(Tokens[i] + "=> Token não Cadastrado na Tabela!!!!!!");
                    }
                }
            }
        } else {
            System.out.println(Estado + "=> Estado não Cadastrado na Tabela!!!!!!");
        }
    }

    public String Analise(String[] Tokens) {
        int ip;
        String x = "", a = "";
        Tokens = Remove("tk_comentario", Tokens);
        ArrayList<String> Erro = new ArrayList();
        TokensComErro = new ArrayList();
        Pilha.push("£");
        Pilha.push(Estados.get(EstadoInicial));
        Tokens[Tokens.length - 1] = "£";
        ip = 0;

        while (!x.equals("£") && Erro.size() < 20) {
            x = Pilha.peek();
            a = Tokens[ip];

            if (BuscaTerminal(x) != -1 || x.equals("£")) {
                if (x.equals(a)) {
                    x = Pilha.pop();
                    System.out.println(x);
                    ip++;
                } else {
                    x = Pilha.pop();
                    /////ip++;
                    Erro.add("Erro Era Esperado " + x + " Foi Recebido " + a + " Erro no Token " + ip);
                    getTokensComErro().add(ip);
                }
            } else {
                try {
                    String Regra = BuscaRegra(x, a);
                    if (!Regra.equals("")) {/*Existe na tabela sintática regra x que produz a*/
                        x = Pilha.pop();
                        /*Empilhar Inverso*/
                        String[] Regras = Regra.split(" ");
                        for (int i = Regras.length - 1; i >= 0; i--) {
                            Pilha.push(Regras[i]);
                        }
                    } else if (BuscaRegra(x, "tk_vazio").equals("tk_vazio")) {
                        x = Pilha.pop();
                    } else {
                        x = Pilha.pop();
                        /////ip++;
                        Erro.add("Erro Era Esperado " + x + " Foi Recebido " + a + " Erro no Token " + ip);
                        getTokensComErro().add(ip);
                    }
                } catch (Exception ex) {
                     x = Pilha.pop();
                    Erro.add("Erro Era Esperado " + x + " Foi Recebido " + a + " Erro no Token " + ip);
                    getTokensComErro().add(ip);
                }
            }

        }
        String Retorno = "";
        if (!Erro.isEmpty()) {
            for (String string : Erro) {
                Retorno += "\n " + string;
            }
            Retorno += "\n O Programa Contém " + Erro.size() + " Erros!";
        } else {
            Retorno += "\n Sem Erros!!!!!";
        }
        return Retorno;
    }

    protected int BuscaTerminal(String x) {
        return BuscaToken(x);
    }

    protected int BuscaToken(String x) {
        int Pos = 0;
        while (Pos < Tokens.size() && !Tokens.get(Pos).equals(x)) {
            Pos++;
        }
        return Pos < Tokens.size() ? Pos : -1;
    }

    protected int BuscaEstado(String x) {
        int Pos = 0;
        while (Pos < Estados.size() && !Estados.get(Pos).equals(x)) {
            Pos++;
        }
        return Pos < Estados.size() ? Pos : -1;
    }

    protected String BuscaRegra(String x, String a) {
        int PosEstado = BuscaEstado(x);
        int PosToken = BuscaToken(a);
        return Regras[PosEstado][PosToken];
    }

    /**
     * @return the TokensComErro
     */
    public ArrayList<Integer> getTokensComErro() {
        return TokensComErro;
    }

    protected String[] Remove(String tk, String[] Tokens) {
        ArrayList<String> Aux = new ArrayList();
        for (String Token : Tokens) {
            if(Token == null){
                Aux.add("");
            }else
            if (!Token.equalsIgnoreCase(tk)) {
                Aux.add(Token);
            }
        }
        String[] Retorno = new String[Aux.size()];
        for (int i = 0; i < Aux.size(); i++) {
            Retorno[i] = Aux.get(i);
        }
        return Retorno;
    }

}
