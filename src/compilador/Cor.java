/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador;

import compilador.AnaliseLexica.NodeTransicao;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.concurrent.Task;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;
import org.reactfx.Subscription;

/**
 *
 * @author Aluno
 */
public class Cor {

    private Matcher matcher;
    private CodeArea txbCodigo;
    private ExecutorService executor;

    public Cor(CodeArea txbCodigo) {
        this.txbCodigo = txbCodigo;
        Inicializa();
        InicializaTextColorSync();
    }

    public void Inicializa() {
        matcher = new Matcher(txbCodigo);
    }

    public void InicializaTextColorSync() {
        Subscription cleanupWhenNoLongerNeedIt = txbCodigo.plainTextChanges()
                .successionEnds(Duration.ofMillis(500))
                .subscribe(ignore -> txbCodigo.setStyleSpans(0, computeHighlighting()));
    }

    public void InicializaTextColorAsync() {
        executor = Executors.newSingleThreadExecutor();
        Subscription cleanupWhenDone = txbCodigo.plainTextChanges()
                .successionEnds(Duration.ofMillis(500))
                .supplyTask(this::computeHighlightingAsync)
                .awaitLatest(txbCodigo.plainTextChanges())
                .filterMap(t -> {
                    if (t.isSuccess()) {
                        return Optional.of(t.get());
                    } else {
                        t.getFailure().printStackTrace();
                        return Optional.empty();
                    }
                })
                .subscribe(this::applyHighlighting);
    }

    private Task<StyleSpans<Collection<String>>> computeHighlightingAsync() {
        String text = txbCodigo.getText();
        Task<StyleSpans<Collection<String>>> task = new Task<StyleSpans<Collection<String>>>() {
            @Override
            protected StyleSpans<Collection<String>> call() throws Exception {
                return computeHighlighting();
            }
        };
        executor.execute(task);
        return task;
    }

    private void applyHighlighting(StyleSpans<Collection<String>> highlighting) {
        txbCodigo.setStyleSpans(0, highlighting);
    }

    public void SetTextColor(int linhaInicial, int linhafinal) {
        String[] Texto = txbCodigo.getText().split("\n");
        String AntesDaLinha = "", NaLinha = "";
        int x;
        for (x = 0; x < linhaInicial; x++) {
            AntesDaLinha += Texto[x];
        }
        for (; x < linhafinal; x++) {
            NaLinha += Texto[x];
        }
        int Comeco = AntesDaLinha.length();
        int Final = (NaLinha.length() + AntesDaLinha.length());
        Inicializa();
        getMatcher().addMatch(Comeco, Final, "string");
    }

    public void SetTextColor(ArrayList<NodeTransicao> nTransicao) {
        Inicializa();
        for (int i = 0; i < nTransicao.size(); i++) {
            String string = nTransicao.get(i).getToken();
            String style = "";
            boolean match = false;
            if (nTransicao.get(i).getEstado().equalsIgnoreCase("Token Aceito") || nTransicao.get(i).getEstado().equalsIgnoreCase("Token Aceito - Retrocede")) {
                if (AnalisaTokens(string, "tk_inteiro", "tk_letra", "tk_cientifico", "tk_pergunta", "tk_enquanto")) {
                    style = "keyword";
                    match = true;
                } else if (AnalisaTokens(string, "tk_atribuicao", "tk_adicao", "tk_subtracao", "tk_multiplicacao", "tk_divisao", "tk_resto_divisao",
                        "tk_maior", "tk_menor", "tk_maior_igual", "tk_menor_igual", "tk_igual", "tk_negacao", "tk_diferente_igual", "tk_or",
                        "tk_and", "tk_deslocar1bitdireita", "tk_deslocar1bitesquerda", "tk_atribuicao", "tk_incrementa_atribuicao",
                        "tk_decrementa_atribuicao", "tk_virgula", "tk_ponto_virgula")) {
                    style = "brace";
                    match = true;
                } else if (AnalisaTokens(string, "tk_abrir_chave", "tk_fechar_chave")) {
                    style = "paren";
                    match = true;
                } else if (AnalisaTokens(string, "tk_abrir_parentese", "tk_fechar_parentese", "tk_abrir_colchete", "tk_fechar_colchete")) {
                    style = "paren";
                    match = true;
                } else if (AnalisaTokens(string, "tk_caracter")) {
                    style = "char";
                    match = true;
                } else if (AnalisaTokens(string, "tk_comentario")) {
                    style = "comment";
                    match = true;
                }
            }else{
                style = "erro";
                match = true;
            }
            if (match) {
                getMatcher().addMatch(nTransicao.get(i).getInicio(), nTransicao.get(i).getFim(), style);
            }
        }
    }

    public boolean AnalisaTokens(String Token, String... Tokens) {
        int i = 0;
        while (i < Tokens.length && !Token.equalsIgnoreCase(Tokens[i])) {
            i++;
        }
        return i < Tokens.length;
    }

    public String[] Construir(String string) {
        ArrayList<String> Texto2 = new ArrayList();
        string = Substituir(string, '\n', " ");
        string = Substituir(string, '\t', " ");
        string = CorrigeString(string, ' ');
        for (String object : string.split(" ")) {
            Texto2.add(object);
        }
        String[] Texto3 = new String[Texto2.size()];
        for (int i = 0; i < Texto2.size(); i++) {
            Texto3[i] = Texto2.get(i);
        }
        return Texto3;
    }

    private String Substituir(String string, char CaracterS, String CaracterD) {
        String Retorno = "";
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) != CaracterS) {
                Retorno += string.charAt(i);
            } else {
                Retorno += CaracterD;
            }
        }
        if (Retorno.isEmpty()) {
            return string;
        }
        return Retorno;
    }

    public String CorrigeString(String string, char Caracter) {
        String Retorno = "";
        String Buffer = "";
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == Caracter) {
                Buffer += Caracter;
            } else {
                if (Buffer.isEmpty()) {
                    Retorno += string.charAt(i);
                } else {
                    Retorno += Buffer + Caracter;
                    Retorno += string.charAt(i);
                    Buffer = "";
                }
            }
        }
        if (!Buffer.isEmpty()) {
            Retorno += Buffer + Caracter;
        }
        return Retorno;
    }

    public StyleSpans<Collection<String>> computeHighlighting() {

        int lastKwEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();
        try {
            while (matcher.HasNext()) {
                Match mat = matcher.Next();
                String styleClass = mat.getStyle();
                int CaracterFinal, CaracterInicial = mat.getPosCaracterInicial();
                if (mat.getPosCaracterFinal() > 1) {
                    if (mat.getPosCaracterFinal() + 1 < matcher.getTexto().length()) {
                        CaracterFinal = mat.getPosCaracterFinal() + 1;
                    } else {
                        lastKwEnd -= 1;
                        CaracterInicial -= 1;
                        CaracterFinal = mat.getPosCaracterFinal();
                    }
                } else {
                    if (matcher.getTexto().length() > 0) {
                        CaracterFinal = 1;
                    } else {
                        CaracterFinal = 0;
                    }
                }
                spansBuilder.add(Collections.emptyList(), CaracterInicial - lastKwEnd);
                spansBuilder.add(Collections.singleton(styleClass), CaracterFinal
                        - CaracterInicial);
                lastKwEnd = CaracterFinal;
            }
            int Final = matcher.getTexto().length() - lastKwEnd - 1;
            spansBuilder.add(Collections.emptyList(), Final >= 0 ? Final : matcher.getTexto().length());
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return spansBuilder.create();
    }

    /*
    private static StyleSpans<Collection<String>> computeHighlighting(int Final) {
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();
        try {

            int lastKwEnd = 0;
            String styleClass;
            if (flag == 1) {
                styleClass = "string";
                flag = 0;
            } else {
                styleClass = "erro";
                flag = 1;
            }
            spansBuilder.add(Collections.emptyList(), lastKwEnd);
            spansBuilder.add(Collections.singleton(styleClass), Final);

        } catch (Exception ex) {
            System.out.println("Erro");
        }
        return spansBuilder.create();
    }*/
    /**
     * @return the matcher
     */
    public Matcher getMatcher() {
        return matcher;
    }

    @Override
    protected void finalize() throws Throwable {
        executor.shutdown();
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }

    public void Refresh() {
        Inicializa();
    }

    public static class Matcher {

        private ArrayList<Match> Matches = new ArrayList();
        private int PosMatcher;
        private final CodeArea Texto;

        public Matcher(CodeArea Texto) {
            this.Texto = Texto;
            Inicializa();
        }

        public void Inicializa() {
            PosMatcher = 0;
        }

        public Match Next() {
            return HasNext() ? getMatches().get(PosMatcher++) : null;
        }

        public boolean HasNext() {
            return getPosMatcher() < getMatches().size();
        }

        public void addMatch(int PosCaracterInicial, int PosCaracterFinal, String style) {
            getMatches().add(new Match(PosCaracterInicial, PosCaracterFinal, style));
        }

        /**
         * @return the Matches
         */
        public ArrayList<Match> getMatches() {
            return Matches;
        }

        /**
         * @return the Texto
         */
        public String getTexto() {
            return Texto.getText();
        }

        /**
         * @return the PosMatcher
         */
        public int getPosMatcher() {
            return PosMatcher;
        }
    }

    private static class Match {

        private int PosCaracterInicial;

        public Match(int PosCaracterInicial, int PosCaracterFinal, String style) {
            this.PosCaracterInicial = PosCaracterInicial;
            this.PosCaracterFinal = PosCaracterFinal;
            this.style = style;
        }
        private int PosCaracterFinal;
        private String style;

        public Match() {
        }

        /**
         * @return the PosCaracterInicial
         */
        public int getPosCaracterInicial() {
            return PosCaracterInicial;
        }

        /**
         * @return the PosCaracterFinal
         */
        public int getPosCaracterFinal() {
            return PosCaracterFinal;
        }

        /**
         * @return the style
         */
        public String getStyle() {
            return style;
        }
    }
}
