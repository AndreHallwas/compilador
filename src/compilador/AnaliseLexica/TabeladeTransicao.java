/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseLexica;

import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public abstract class TabeladeTransicao {

    /*
    *Automatos Finitos Deterministicos Regulares
     */
    protected int TamEstados;
    protected int EstadoAtual;
    protected int EstadoInicial;
    protected int TamSimbulos;
    protected ArrayList<Character> Simbulos;
    protected ArrayList<Integer> EstadosFinais;
    protected int[][] Transicoes;
    protected String Caracteres;
    protected String ResultadoDetalhado;
    protected ArrayList<NodeTransicao> NTransicao;

    public TabeladeTransicao(int TamEstados, int TamSimbulos, int EstadoInicial) {
        Inicializa(TamEstados, TamSimbulos, EstadoInicial);
    }

    protected void Inicializa(int TamEstados, int TamSimbulos, int EstadoInicial) {
        Simbulos = new ArrayList();
        EstadosFinais = new ArrayList();

        InicializaTransicoes(TamEstados, TamSimbulos);

        this.TamEstados = TamEstados;
        this.TamSimbulos = TamSimbulos;
        this.EstadoInicial = EstadoInicial;
        this.EstadoAtual = EstadoInicial;
        this.Caracteres = "";
    }

    protected void InicializaTransicoes(int Estados, int Simbulos) {
        Transicoes = new int[Estados][];
        for (int i = 0; i < Transicoes.length; i++) {
            Transicoes[i] = new int[Simbulos];
            for (int j = 0; j < Simbulos; j++) {
                Transicoes[i][j] = -1;
            }
        }
    }

    public void addSimbulo(Character Simbulo) {
        if (Simbulos != null && Simbulos.size() <= TamSimbulos) {
            Simbulos.add(Simbulo);
        }
    }

    public void addEstadoFinal(int Estado) {
        if (EstadosFinais != null && EstadosFinais.size() <= TamEstados) {
            EstadosFinais.add(Estado);
        }
    }

    public void addTransicao(int Estado, Character Simbulo, int TransicaoEstado) {
        if (Estado <= TamEstados) {
            int PosSimbulo = Busca_Exaustiva_Simbulo(Simbulo);
            if (PosSimbulo != -1) {
                Transicoes[Estado][PosSimbulo] = TransicaoEstado;
            } else {
                System.out.println("Simbulo NÃ£o Cadastrado na Tabela!!!!!!");
            }
        }
    }

    public void addTransicao(int Estado, int TransicaoEstado, Character... Simbulo) {
        if (Estado <= TamEstados) {
            if (Simbulo != null) {
                int Tam = Simbulo.length;
                for (int i = 0; i < Tam; i++) {
                    int PosSimbulo = Busca_Exaustiva_Simbulo(Simbulo[i]);
                    if (PosSimbulo != -1) {
                        Transicoes[Estado][PosSimbulo] = TransicaoEstado;
                    } else {
                        System.out.println("Simbulo NÃ£o Cadastrado na Tabela!!!!!!");
                    }
                }
            }
        }
    }

    protected int Busca_Exaustiva_Simbulo(Character Simbulo) {
        int Pos = 0;
        while (Pos < Simbulos.size() && !Simbulos.get(Pos).equals(Simbulo)) {
            Pos++;
        }
        return Pos < Simbulos.size() ? Pos : -1;
    }

    protected int Busca_Exaustiva_Estado_Final(int Estado) {
        int Pos = 0;
        while (Pos < EstadosFinais.size() && !EstadosFinais.get(Pos).equals(Estado)) {
            Pos++;
        }
        return Pos < EstadosFinais.size() ? Pos : -1;
    }

    public String ClassificaResultado(int Resultado) {
        /*
        *-1 = NÃ£o Existe na Tabela
        *0 = Avaliando
        *1 = Rejeitado
        *2 = Aceito
        *3 = Aceito e Retrocede
         */
        return (Resultado == -1) ? "NÃ£o Existe na Tabela"
                : (Resultado == 0) ? "Avaliando"
                        : (Resultado == 0) ? "Rejeitado"
                                : (Resultado == 0) ? "Aceito" : "Resultado InvÃ¡lido";
    }

    public String Analise(String Programa, String Param) {
        TabeladeTokens tk = new TabeladeTokens();
        NTransicao = new ArrayList();

        String AuxResultado = "";
        int Saida = 0;
        for (int i = 0, Linhas = 1, Inicio = i; i < Programa.length(); i++) {
            if (Programa.charAt(i) != '\n' && Programa.charAt(i) != '\t') {
                if(Saida != 0){
                    Inicio++;
                }
                Saida = ProximoSimbulo(Programa.charAt(i));
                if (Saida == 1 || Saida == -1) {
                    if (Programa.charAt(i) != ' ') {
                        NTransicao.add(new NodeTransicao(getCaracteres(), "Token Rejeitado",
                                Inicio, i, Linhas));
                        Inicio = i;
                    } else if (getCaracteres().charAt(0) != ' ') {
                        NTransicao.add(new NodeTransicao(getCaracteres().substring(0, getCaracteres().length() - 1),
                                "Token Rejeitado", Inicio, --i, Linhas));
                        Inicio = i;
                    }
                    EstadoAtual = EstadoInicial;
                    Caracteres = "";
                } else if (Saida == 2) {
                    if (getCaracteres().charAt(getCaracteres().length() - 1) == ' ') {
                        AuxResultado = tk.Busca_Tokens(getCaracteres().substring(0, getCaracteres().length() - 1));
                        NTransicao.add(new NodeTransicao(getCaracteres().substring(0, getCaracteres().length() - 1),
                                AuxResultado, "Token Aceito", Inicio, --i, Linhas));
                        Inicio = i;
                    } else {
                        AuxResultado = tk.Busca_Tokens(getCaracteres());
                        NTransicao.add(new NodeTransicao(getCaracteres(), AuxResultado, "Token Aceito",
                                Inicio, i, Linhas));
                        Inicio = i;
                    }
                    EstadoAtual = EstadoInicial;
                    Caracteres = "";
                } else if (Saida == 3) {
                    AuxResultado = tk.Busca_Tokens(getCaracteres().substring(0, getCaracteres().length() - 1));
                    NTransicao.add(new NodeTransicao(getCaracteres().substring(0, getCaracteres().length() - 1),
                            AuxResultado, "Token Aceito - Retrocede", Inicio, --i, Linhas));
                    EstadoAtual = EstadoInicial;
                    Caracteres = "";
                    Inicio = i;
                }
            } else {
                if(Programa.charAt(i) == '\n'){
                    Linhas++;
                }
                Inicio++;
            }
        }
        if (Param != null) {
            if (Param.equals("d")) {
                return getDetalhado();
            } else if (Param.equals("p")) {
                System.out.println(getDetalhado());
            } else if (Param.equals("e")) {
                return getErros();
            }
        }
        return getSimples();
    }
    
    public int getLinha(String Token){
        int Resultado = -1;
        int Pos = 0;
        while (Pos < NTransicao.size() && !NTransicao.get(Pos).getToken().equalsIgnoreCase(Token))
            Pos++;
        if(Pos < NTransicao.size()){
            Resultado = NTransicao.get(Pos).getLinha();
        }
        return Resultado;
    }

    public String getDetalhado() {
        String Resultado = "";
        for (NodeTransicao node : NTransicao) {
            Resultado += node.Detalhado();
        }
        return Resultado;
    }

    public String getSimples() {
        String Resultado = "";
        for (NodeTransicao node : NTransicao) {
            Resultado += node.Simples();
        }
        return Resultado;
    }

    public String getErros() {
        String Resultado = "";
        for (NodeTransicao node : NTransicao) {
            if (!node.getEstado().contains("Token Aceito")) {
                Resultado += node.Detalhado();
            }
        }
        return Resultado;
    }

    public String[] getTokens() {
        ArrayList<String> AuxTokens = new ArrayList();
        if (NTransicao != null && !NTransicao.isEmpty()) {
            for (NodeTransicao node : NTransicao) {
                AuxTokens.add(node.getToken());
            }
        }
        String[] Tokens = new String[AuxTokens.size() + 1];
        for (int i = 0; i < AuxTokens.size(); i++) {
            Tokens[i] = AuxTokens.get(i);
        }
        return Tokens;
    }

    public int ProximoSimbulo(Character Simbulo) {

        int Resposta = 0;
        int PosSimbulo = Busca_Exaustiva_Simbulo(Simbulo);
        int ProximoEstado;
        Caracteres += Simbulo;
        if (PosSimbulo == -1) {
            //alt+156 Simbulo que representa nada
            PosSimbulo = Busca_Exaustiva_Simbulo('£');
            /**
             * TransiÃ§Ã£o Vazia
             */
        }
        if (PosSimbulo != -1) {
            ProximoEstado = Transicoes[EstadoAtual][PosSimbulo];
            if (ProximoEstado != -1) {
                EstadoAtual = ProximoEstado;
                if (Busca_Exaustiva_Estado_Final(ProximoEstado) != -1) {
                    Resposta = 2;
                }
            } else {
                ProximoEstado = Transicoes[EstadoAtual][Busca_Exaustiva_Simbulo('£')];
                if (ProximoEstado != -1) {
                    EstadoAtual = ProximoEstado;
                    Resposta = 3;
                } else {
                    Resposta = 1;
                }
            }
        } else {
            Resposta = -1;
        }
        return Resposta;
    }

    public int ProximoFinal() {

        int Resposta = 0;
        int ProximoEstado;
        ProximoEstado = Transicoes[EstadoAtual][Busca_Exaustiva_Simbulo('£')];
        if (ProximoEstado != -1) {
            EstadoAtual = ProximoEstado;
            Resposta = 2;
        } else {
            Resposta = 1;
        }
        return Resposta;
    }

    public ArrayList<NodeTransicao> getNTransicao() {
        return NTransicao;
    }

    /**
     * @return the Caracteres
     */
    public String getCaracteres() {
        return Caracteres;
    }

}
