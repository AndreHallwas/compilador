/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseLexica;

/**
 *
 * @author Raizen
 */
public class AutomatoLexico extends TabeladeTransicao{
    
    public AutomatoLexico() {
        super(15, 57, 0);
        addEstadoFinal(2);
        add('a','b','c','d','e','f','g','h','i','j','k','l',
                'm','n','o','p','q','r','s','t','u','v','w','x','y','z');
        add('0','1','2','3','4','5','6','7','8','9');
        add('(',')',']','[','{','}',',',';',"'".charAt(0));
        add('>','<','=','!');
        add('.','E','-','+','*','/','%');
        add('£');/////Alt+156;
        addTransicao(0, '=', 1);
        addTransicao(1, '=', 2);
        addTransicao(0,1,'>','<','=','!','-','+');
        addTransicao(1,2,'=','£');
        addTransicao(0,3,'a','b','c','d','e','f','g','h','i','j','k','l',
                'm','n','o','p','q','r','s','t','u','v','w','x','y','z');
        addTransicao(3,3,'a','b','c','d','e','f','g','h','i','j','k','l',
                'm','n','o','p','q','r','s','t','u','v','w','x','y','z',
                '0','1','2','3','4','5','6','7','8','9');
        addTransicao(3, '£', 2);
        addTransicao(0,2,'(',')',']','[','{','}',',',';','*','%');
        addTransicao(0,4,'0','1','2','3','4','5','6','7','8','9');
        addTransicao(4,4,'0','1','2','3','4','5','6','7','8','9');
        addTransicao(4, '.', 5);
        addTransicao(4, 'E', 7);
        addTransicao(4, '£', 2);
        addTransicao(5,6,'0','1','2','3','4','5','6','7','8','9');
        addTransicao(6,6,'0','1','2','3','4','5','6','7','8','9');
        addTransicao(6, '£', 2);
        addTransicao(6, 'E', 7);
        addTransicao(7,8,'+','-');
        addTransicao(7,9,'0','1','2','3','4','5','6','7','8','9');
        addTransicao(8,9,'0','1','2','3','4','5','6','7','8','9');
        addTransicao(9,9,'0','1','2','3','4','5','6','7','8','9');
        addTransicao(9, '£', 2);
        /*addTransicao(0, "'".charAt(0), 2);*/
        addTransicao(0, "'".charAt(0), 10);
        addTransicao(10,11,'a','b','c','d','e','f','g','h','i','j','k','l','m','n',
                'o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4',
                '5','6','7','8','9','(',')',']','[','{','}',';','*','/','%','>',
                '<','=','!','-','+','.','*','/','%'/*,'£'*/);
        addTransicao(11, "'".charAt(0), 2);
        
        addTransicao(0, '/', 12);
        addTransicao(12, '£', 2);
        addTransicao(12, '*', 13);
        addTransicao(13,13,'a','b','c','d','e','f','g','h','i','j','k','l',
                'm','n','o','p','q','r','s','t','u','v','w','x','y','z',
                '0','1','2','3','4','5','6','7','8','9');
        addTransicao(13, '*', 14);
        addTransicao(14, '/', 2);
    }
    
    public void add(Character... Params){
        if(Params != null){
            int Tam = Params.length;
            for (int i = 0; i < Tam; i++) {
                addSimbulo(Params[i]);
            }
        }
    }
    
}
