/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseLexica;

import com.sun.javafx.scene.control.skin.LabeledText;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;

/**
 *
 * @author Raizen
 */
public class NodeTransicao {

    private String Caracteres;
    private String Token;
    private String Estado;
    private int Inicio;
    private int Fim;
    private int Linha;

    public NodeTransicao() {
    }

    public NodeTransicao(String Caracteres, String Estado, int Inicio, int Fim, int Linha) {
        this.Caracteres = Caracteres;
        this.Estado = Estado;
        this.Inicio = Inicio;
        this.Fim = Fim;
        this.Linha = Linha;
    }

    public NodeTransicao(String Caracteres, String Estado, int Inicio, int Fim) {
        this.Caracteres = Caracteres;
        this.Estado = Estado;
        this.Inicio = Inicio;
        this.Fim = Fim;
    }

    public NodeTransicao(String Caracteres, String Token, String Estado, int Inicio, int Fim) {
        this.Caracteres = Caracteres;
        this.Token = Token;
        this.Estado = Estado;
        this.Inicio = Inicio;
        this.Fim = Fim;
    }

    public NodeTransicao(String Caracteres, String Token, String Estado, int Inicio, int Fim, int Linha) {
        this.Caracteres = Caracteres;
        this.Token = Token;
        this.Estado = Estado;
        this.Inicio = Inicio;
        this.Fim = Fim;
        this.Linha = Linha;
    }

    /**
     * @return the Caracteres
     */
    public String getCaracteres() {
        return Caracteres;
    }

    /**
     * @param Caracteres the Caracteres to set
     */
    public void setCaracteres(String Caracteres) {
        this.Caracteres = Caracteres;
    }

    /**
     * @return the Token
     */
    public String getToken() {
        return Token;
    }

    /**
     * @param Token the Token to set
     */
    public void setToken(String Token) {
        this.Token = Token;
    }

    /**
     * @return the Estado
     */
    public String getEstado() {
        return Estado;
    }

    /**
     * @param Estado the Estado to set
     */
    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    /**
     * @return the Inicio
     */
    public int getInicio() {
        return Inicio;
    }

    /**
     * @param Inicio the Inicio to set
     */
    public void setInicio(int Inicio) {
        this.Inicio = Inicio;
    }

    /**
     * @return the Fim
     */
    public int getFim() {
        return Fim;
    }

    /**
     * @param Fim the Fim to set
     */
    public void setFim(int Fim) {
        this.Fim = Fim;
    }

    @Override
    public String toString() {
        if (getToken() != null && !getToken().trim().isEmpty()) {
            return getCaracteres() + " " + getToken() + " " + getEstado() + " " + getInicio() + "-" + getFim() + " Linha: " + getLinha() + "\n";
        }
        return getCaracteres() + " " + getEstado() + " " + getInicio() + "-" + getFim() + " Linha: " + getLinha() + "\n";
    }

    public TreeItem getTree() {
        TreeItem raiz;
        if (getEstado().equalsIgnoreCase("Token Rejeitado")) {
            LabeledText text = new LabeledText(new Label());
            text.setText(getCaracteres());
            text.setStyle("-fx-font-style: italic; -fx-fill: red; -fx-font-weight: bold; -fx-underline: true;");
            raiz = new TreeItem(text);
        } else {
            raiz = new TreeItem(getCaracteres());
        }
        raiz.getChildren().add(new TreeItem("Token - " + getToken()));
        raiz.getChildren().add(new TreeItem("Estado - " + getEstado()));
        raiz.getChildren().add(new TreeItem("Inicio - " + getInicio()));
        raiz.getChildren().add(new TreeItem("Fim - " + getFim()));
        raiz.getChildren().add(new TreeItem("Linha - " + getLinha()));
        return raiz;
    }

    public String Detalhado() {
        return toString();
    }

    public String Simples() {
        if (getToken() != null && !getToken().trim().isEmpty()) {
            return getToken() + "\n";
        }
        return getCaracteres() + " " + getEstado() + "\n";
    }

    /**
     * @return the Linha
     */
    public int getLinha() {
        return Linha;
    }

    /**
     * @param Linha the Linha to set
     */
    public void setLinha(int Linha) {
        this.Linha = Linha;
    }

}
