/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseLexica;

import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public abstract class TabeladeTransicaoND {

    /*
    *Automatos Finitos Não Deterministicos Regulares
     */
    protected int TamEstados;
    protected int[] EstadoAtual;
    protected int EstadoInicial;
    protected int TamSimbulos;
    protected ArrayList<Character> Simbulos;
    protected ArrayList<Integer> EstadosFinais;
    protected int[][][] Transicoes;
    protected String Caracteres;
    protected String ResultadoDetalhado;
    protected ArrayList<NodeTransicao> NTransicao;

    public TabeladeTransicaoND(int TamEstados, int TamSimbulos, int EstadoInicial) {
        Inicializa(TamEstados, TamSimbulos, EstadoInicial);
    }

    protected void Inicializa(int TamEstados, int TamSimbulos, int EstadoInicial) {
        Simbulos = new ArrayList();
        EstadosFinais = new ArrayList();

        this.TamEstados = TamEstados;
        this.TamSimbulos = TamSimbulos;
        this.EstadoInicial = EstadoInicial;
        this.Caracteres = "";

        InicializaEstadoAtual();
        InicializaTransicoes(TamEstados, TamSimbulos);
    }

    protected void InicializaEstadoAtual() {
        EstadoAtual = new int[TamEstados];
        for (int i = 0; i < TamEstados; i++) {
            EstadoAtual[i] = -1;
        }
        EstadoAtual[EstadoInicial] = 1;
    }

    protected void InicializaTransicoes(int Estados, int Simbulos) {
        Transicoes = new int[Estados][][];
        for (int i = 0; i < Transicoes.length; i++) {
            Transicoes[i] = InicializaMatriz(Simbulos, Estados);
        }
    }

    public void addSimbulo(Character Simbulo) {
        if (Simbulos != null && Simbulos.size() <= TamSimbulos) {
            Simbulos.add(Simbulo);
        }
    }

    public void addEstadoFinal(int Estado) {
        if (EstadosFinais != null && EstadosFinais.size() <= TamEstados) {
            EstadosFinais.add(Estado);
        }
    }

    public void addTransicao(int Estado, Character Simbulo, int TransicaoEstado) {
        if (Estado <= TamEstados) {
            int PosSimbulo = Busca_Exaustiva_Simbulo(Simbulo);
            if (PosSimbulo != -1) {
                Transicoes[Estado][PosSimbulo][TransicaoEstado] = 1;
            } else {
                System.out.println("Simbulo Não Cadastrado na Tabela!!!!!!");
            }
        }
    }

    public void addTransicao(int Estado, int TransicaoEstado, Character... Simbulo) {
        if (Estado <= TamEstados) {
            if (Simbulo != null) {
                int Tam = Simbulo.length;
                for (int i = 0; i < Tam; i++) {
                    int PosSimbulo = Busca_Exaustiva_Simbulo(Simbulo[i]);
                    if (PosSimbulo != -1) {
                        Transicoes[Estado][PosSimbulo][TransicaoEstado] = 1;
                    } else {
                        System.out.println("Simbulo Não Cadastrado na Tabela!!!!!!");
                    }
                }
            }
        }
    }

    protected int Busca_Exaustiva_Simbulo(Character Simbulo) {
        int Pos = 0;
        while (Pos < Simbulos.size() && !Simbulos.get(Pos).equals(Simbulo)) {
            Pos++;
        }
        return Pos < Simbulos.size() ? Pos : -1;
    }

    protected int Busca_Exaustiva_Estado_Final(int Estado) {
        int Pos = 0;
        while (Pos < EstadosFinais.size() && !EstadosFinais.get(Pos).equals(Estado)) {
            Pos++;
        }
        return Pos < EstadosFinais.size() ? Pos : -1;
    }

    public String ClassificaResultado(int Resultado) {
        /*
        *-1 = Não Existe na Tabela
        *0 = Avaliando
        *1 = Rejeitado
        *2 = Aceito
        *3 = Aceito e Retrocede
         */
        return (Resultado == -1) ? "Não Existe na Tabela"
                : (Resultado == 0) ? "Avaliando"
                        : (Resultado == 0) ? "Rejeitado"
                                : (Resultado == 0) ? "Aceito" : "Resultado Inválido";
    }

    public String Analise(String Programa, String Param) {
        TabeladeTokens tk = new TabeladeTokens();
        NTransicao = new ArrayList();

        String AuxResultado = "";
        int Saida;
        for (int i = 0, Inicio = i; i < Programa.length(); i++) {
            if (Programa.charAt(i) != '\n' && Programa.charAt(i) != '\t') {
                Saida = ProximoSimbulo(Programa.charAt(i));
                if (Saida == 1 || Saida == -1) {
                    if (Programa.charAt(i) != ' ') {
                        NTransicao.add(new NodeTransicao(getCaracteres(), "Token Rejeitado",
                                Inicio, i));
                        Inicio = i;
                    } else if (getCaracteres().charAt(0) != ' ') {
                        NTransicao.add(new NodeTransicao(getCaracteres().substring(0, getCaracteres().length() - 1),
                                "Token Rejeitado", Inicio, i));
                        Inicio = i;
                    }
                    InicializaEstadoAtual();
                    Caracteres = "";
                } else if (Saida == 2) {
                    if (getCaracteres().charAt(getCaracteres().length() - 1) == ' ') {
                        AuxResultado = tk.Busca_Tokens(getCaracteres().substring(0, getCaracteres().length() - 1));
                        NTransicao.add(new NodeTransicao(getCaracteres().substring(0, getCaracteres().length() - 1),
                                AuxResultado, "Token Aceito", Inicio, i));
                        Inicio = i;
                    } else {
                        AuxResultado = tk.Busca_Tokens(getCaracteres());
                        NTransicao.add(new NodeTransicao(getCaracteres(), AuxResultado, "Token Aceito",
                                Inicio, i));
                        Inicio = i;
                    }
                    InicializaEstadoAtual();
                    Caracteres = "";
                } else if (Saida == 3) {
                    AuxResultado = tk.Busca_Tokens(getCaracteres().substring(0, getCaracteres().length() - 1));
                    NTransicao.add(new NodeTransicao(getCaracteres().substring(0, getCaracteres().length() - 1),
                            AuxResultado, "Token Aceito - Retrocede", Inicio, i - 1));
                    InicializaEstadoAtual();
                    Caracteres = "";
                    i--;
                    Inicio = i;
                }
            } else {
                Inicio++;
            }
        }
        if (Param != null) {
            if (Param.equals("d")) {
                return getDetalhado();
            } else if (Param.equals("p")) {
                System.out.println(getDetalhado());
            }
        }
        return getSimples();
    }

    public String getDetalhado() {
        String Resultado = "";
        for (NodeTransicao node : NTransicao) {
            Resultado += node.Detalhado();
        }
        return Resultado;
    }

    public String getSimples() {
        String Resultado = "";
        for (NodeTransicao node : NTransicao) {
            Resultado += node.Simples();
        }
        return Resultado;
    }

    public int[] InicializaVetor(int Tam) {
        int[] Var = new int[Tam];
        for (int i = 0; i < Tam; i++) {
            Var[i] = -1;
        }
        return Var;
    }

    public int[][] InicializaMatriz(int x, int y) {
        int[][] Var = new int[x][];
        for (int i = 0; i < x; i++) {
            Var[i] = InicializaVetor(y);
        }
        return Var;
    }

    public int ProximoSimbulo(Character Simbulo) {

        int Resposta = 0;
        int PosSimbulo = Busca_Exaustiva_Simbulo(Simbulo);
        int ProximoEstado;
        Caracteres += Simbulo;
        if (PosSimbulo == -1) {
            //alt+156 Simbulo que representa nada
            PosSimbulo = Busca_Exaustiva_Simbulo('£');
            /**
             * Transição Vazia
             */
        }
        if (PosSimbulo != -1) {
            /*Inicializa e Zera os estados*/
            int[][] EstadosAtuais = InicializaMatriz(TamEstados, 2);

            /*Analisando Proximos Estados*/
            for (Integer Estadoatual : EstadoAtual) {
                if (Estadoatual != -1) {
                    for (int[] TransicoesEstadoAtual : Transicoes[Estadoatual]) {
                        for (int EstadodaTransicao = 0; EstadodaTransicao < TransicoesEstadoAtual.length; EstadodaTransicao++) {
                            if (TransicoesEstadoAtual[EstadodaTransicao] == 1) {
                                ProximoEstado = EstadodaTransicao;
                                if (ProximoEstado != -1) {
                                    EstadosAtuais[ProximoEstado][0] = 1;
                                    if (Busca_Exaustiva_Estado_Final(ProximoEstado) != -1) {
                                        EstadosAtuais[ProximoEstado][1] = 2;
                                    }
                                } else {
                                    for (int ProximoEstadoVazio = 0, Pos = Busca_Exaustiva_Simbulo('£');
                                            ProximoEstadoVazio < Transicoes[Estadoatual][Pos].length;
                                            ProximoEstadoVazio++) {
                                        ProximoEstado = ProximoEstadoVazio;
                                        if (Transicoes[Estadoatual][Pos][ProximoEstado] != -1) {
                                            EstadosAtuais[ProximoEstado][0] = 1;
                                            EstadosAtuais[ProximoEstado][1] = 3;
                                        } else {
                                            EstadosAtuais[ProximoEstado][1] = 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /*Analisa Respostas, e so deixa os válidos*/
            for (int i = 0; i < EstadosAtuais.length; i++) {
                if (EstadosAtuais[i][1] == 3 || EstadosAtuais[i][1] == 2) {
                    Resposta = EstadosAtuais[i][1];///
                    EstadoAtual[i] = 1;
                } else if (Resposta != 3 && Resposta != 2 && EstadosAtuais[i][1] == 1) {
                    Resposta = EstadosAtuais[i][1];
                    EstadoAtual[i] = -1;
                }
            }
        } else {
            Resposta = -1;
        }
        return Resposta;
    }

    public int ProximoFinal() {

        int Resposta = 0;
        int ProximoEstado;
        for (int Estadoatual : EstadoAtual) {
            for (int i = 0, Pos = Busca_Exaustiva_Simbulo('£'); i < Transicoes[Estadoatual][Pos].length; i++) {
                ProximoEstado = Estadoatual;
                if (ProximoEstado != -1) {
                    EstadoAtual[ProximoEstado] = 1;
                    Resposta = 2;
                } else {
                    if (Resposta != 2) {
                        Resposta = 1;
                    }
                }
            }
        }
        return Resposta;
    }

    /**
     * @return the Caracteres
     */
    public String getCaracteres() {
        return Caracteres;
    }

}
