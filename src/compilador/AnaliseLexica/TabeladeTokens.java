/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseLexica;

import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class TabeladeTokens {

    private ArrayList<NodeLexico> Tokens;

    public TabeladeTokens() {
        InicializaTokens();
    }

    public void InicializaTokens() {
        Tokens = new ArrayList();
        AdicionaTipos();
        AdicionaRepeticao();
        AdicionaExpressaoLogica();
        AdicionaOperadorMatematico();
        AdicionaSeparador();
        AdicionaNumero();
        AdicionaAtribuicao();
        AdicionaDelimitador();
        AdicionaPergunta();
        AdicionaLetra();
        /*AdicionaMain();*/
    }

    private int Busca_Exaustiva(String Palavra) {
        int Pos = 0;
        while (Pos < Tokens.size() && !Tokens.get(Pos).getNome().equals(Palavra)) {
            Pos++;
        }
        return Pos < Tokens.size() ? Pos : -1;
    }

    public String Busca_Tokens(String Palavra) {
        int Pos = Busca_Exaustiva(Palavra);
        return (Pos != -1) ? Tokens.get(Pos).getToken() : Busca_Tokens_Esp(Palavra);
    }
    
    public String Busca_Tokens_Esp(String Palavra) {
        int Pos = Busca_Exaustiva(Palavra.charAt(0)+"");
        String Retorno = "";
        if(Pos != -1){
            Pos = Integer.parseInt(Tokens.get(Pos).getCodigo());
            if(Pos == 3){
                Retorno += "tk_caracter";
            }else if(Pos >= 70 && Pos <= 80){
                Retorno += "tk_numero";
            }else if(Pos == 14){
                Pos = Busca_Exaustiva(Palavra.charAt(1)+"");
                Pos = Integer.parseInt(Tokens.get(Pos).getCodigo());
                if(Pos == 13){
                    Pos = Busca_Exaustiva(Palavra.charAt(Palavra.length()-2)+"");
                    Pos = Integer.parseInt(Tokens.get(Pos).getCodigo());
                    if(Pos == 13){
                        Pos = Busca_Exaustiva(Palavra.charAt(Palavra.length()-1)+"");
                        Pos = Integer.parseInt(Tokens.get(Pos).getCodigo());
                        if(Pos == 14){
                            Retorno += "tk_comentario";
                        }
                    }
                }
            }
        }else{
            Retorno += "tk_id";
        }
        return Retorno;
    }

    private void AdicionaDelimitador() {
        AdicionaToken("1", "(", "tk_abrir_parentese", "Delimitador");
        AdicionaToken("2", ")", "tk_fechar_parentese", "Delimitador");
        AdicionaToken("3", "'", "tk_aspas", "Delimitador");
        AdicionaToken("4", "{", "tk_abrir_chave", "Delimitador");
        AdicionaToken("5", "}", "tk_fechar_chave", "Delimitador");
        AdicionaToken("6", "[", "tk_abrir_colchete", "Delimitador");
        AdicionaToken("7", "]", "tk_fechar_colchete", "Delimitador");
        AdicionaToken("8", ";", "tk_ponto_virgula", "Delimitador");
    }

    private void AdicionaSeparador() {
        AdicionaToken("9", ",", "tk_virgula", "Separador");
    }

    private void AdicionaComentario() {
        AdicionaToken("10", "//", "tk_comentario", "Comentario");
    }

    private void AdicionaOperadorMatematico() {
        AdicionaToken("11", "+", "tk_adicao", "Operador Matemático");
        AdicionaToken("12", "-", "tk_subtracao", "Operador Matemático");
        AdicionaToken("13", "*", "tk_multiplicacao", "Operador Matemático");
        AdicionaToken("14", "/", "tk_divisao", "Operador Matemático");
        AdicionaToken("15", "%", "tk_resto_divisao", "Operador Matemático");
    }

    private void AdicionaExpressaoLogica() {
        AdicionaToken("16", ">", "tk_maior", "Expressão Lógica");
        AdicionaToken("17", "<", "tk_menor", "Expressão Lógica");
        AdicionaToken("18", ">=", "tk_maior_igual", "Expressão Lógica");
        AdicionaToken("19", "<=", "tk_menor_igual", "Expressão Lógica");
        AdicionaToken("20", "==", "tk_igual", "Expressão Lógica");
        AdicionaToken("21", "!", "tk_negacao", "Expressão Lógica");
        AdicionaToken("22", "!=", "tk_diferente_igual", "Expressão Lógica");
        AdicionaToken("23", "||", "tk_or", "Expressão Lógica");
        AdicionaToken("24", "&&", "tk_and", "Expressão Lógica");
        AdicionaToken("25", ">>", "tk_deslocar1bitdireita", "Expressão Lógica");
        AdicionaToken("26", "<<", "tk_deslocar1bitesquerda", "Expressão Lógica");
    }

    private void AdicionaRepeticao() {
        AdicionaToken("27", "while", "tk_enquanto", "Repetição");
    }

    private void AdicionaAtribuicao() {
        AdicionaToken("28", "=", "tk_atribuicao", "Atribuição");
        AdicionaToken("33", "+=", "tk_incrementa_atribuicao", "Atribuição");
        AdicionaToken("34", "-=", "tk_decrementa_atribuicao", "Atribuição");
    }
    
    private void AdicionaMain() {
        AdicionaToken("35", "MAIN", "tk_main", "Main");
    }

    private void AdicionaTipos() {
        AdicionaToken("29", "int", "tk_inteiro", "Tipo");
        AdicionaToken("30", "double", "tk_cientifico", "Tipo");
        AdicionaToken("31", "char", "tk_letra", "Tipo");
    }
    
    private void AdicionaPergunta() {
        AdicionaToken("32", "if", "tk_pergunta", "Pergunta");
        AdicionaToken("36", "else", "tk_else", "Pergunta");
    }

    private void AdicionaNumero() {
        for (int i = 0, j = 70; i < 10; i++, j++) {
            AdicionaToken(j + "", i + "", "tk_numero", "Número");
        }
    }

    private void AdicionaLetra() {
        /*for (int i = 97, j = 115; i <= 122; i++, j++) {
            AdicionaToken(j + "", (char)i + "", "tk_letra", "Letra");
        }*/
    }

    private void AdicionaToken(String Codigo, String Palavra, String Token, String Grupo) {
        if (Tokens != null) {
            Tokens.add(new NodeLexico(Codigo, Palavra, Token, Grupo));
        }
    }

}
