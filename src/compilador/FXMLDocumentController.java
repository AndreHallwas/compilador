/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador;

import compilador.AnaliseLexica.AnaliseLexica;
import compilador.AnaliseLexica.NodeTransicao;
import compilador.AnaliseSemantica.TabeladeSimbulos;
import compilador.AnaliseSintatica.AnaliseSintatica;
import compilador.AnaliseSintatica.TabelaSintatica;
import compilador.CodigoIntermediario.CodigoIntermediario;
import compilador.CodigodeMaquina.CodigodeMaquina;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {

    private Label label;
    @FXML
    private CodeArea txbCodigo;
    @FXML
    private TextArea txbConsole;
    @FXML
    private TextField txbComandoConsole;
    @FXML
    private Button btnOKConsole;

    AnaliseLexica Analise_Lexica = new AnaliseLexica();
    AnaliseSintatica Analise_Sintatica = new AnaliseSintatica();
    Cor cor;
    @FXML
    private TreeView<String> txbTokens;
    @FXML
    private ListView<?> txbSemantica;
    private ArrayList<NodeTransicao> NodeTransicao;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        InicializaBash();
        cor = new Cor(txbCodigo);
        txbCodigo.setParagraphGraphicFactory(LineNumberFactory.get(txbCodigo));
        AdicionaTextoPadrao();
    }

    public void AdicionaTextoPadrao() {
        txbCodigo.replaceText(0, 0, "{\n"
                + "	int a;\n"
                + "	int b;\n"
                + "	a = (5 + 2);\n"
                + "	b = (a + 12);\n"
                + "	while(a <= b){\n"
                + "		if(a <= b){\n"
                + "			a = (a+1);\n"
                + "		}\n"
                + "	}\n"
                + "}");
    }

    public void AdicionaTextoPadrao2() {
        txbCodigo.replaceText(0, 0, "{\n"
                + "	int a;/*aaaa*/\n"
                + "	int b = (15*(12.4+2));\n"
                + "	a = (5+2);\n"
                + "	b = ((5.25E7 / 12) / 12);\n"
                + "	double c;\n"
                + "	char a;\n"
                + "	a = 'a';\n"
                + "	while((a+(b*c)) < 999){\n"
                + "		{\n"
                + "                     /*bbbbbbb*/\n"
                + "			a = (2+2);\n"
                + "		}\n"
                + "		if(a>b){\n"
                + "			a = (a+b);\n"
                + "		}\n"
                + "		double c;\n"
                + "	}\n"
                + "}");
    }

    public void selecinaText(int Posi, int Posf) {
        txbCodigo.showParagraphInViewport(0);
        txbCodigo.moveTo(Posi);
        txbCodigo.selectRange(Posi, Posf);
    }

    public void InicializaBash() {
        txbConsole.appendText("Fipp Bash v 1.28\n");
        txbConsole.appendText(">cmd - Comandos\n");
        txbConsole.appendText(">Para aparecer Cor é necessário digitar um caracter no quadro\n");
        txbConsole.appendText(">\n");
        txbConsole.appendText(">\n");
    }

    public void Comandos() {
        txbConsole.appendText("Fipp Bash v 1.28\n");
        txbConsole.appendText(">cmd - Comandos\n");
        txbConsole.appendText(">cls - Clear\n");
        txbConsole.appendText(">lexico - análise lexica\n");
        txbConsole.appendText(">lexico d - analise com detalhes\n");
        txbConsole.appendText(">lexico p - analise com detalhes no bash do SO\n");
        txbConsole.appendText(">\n");
    }

    @FXML
    private void HandleButtonAbrirArquivo(ActionEvent event) {
    }

    @FXML
    private void HandleButtonSalvarArquivo(ActionEvent event) {
    }

    @FXML
    private void HandleButtonAjudaSobre(ActionEvent event) {
    }

    @FXML
    private void HandleButtonLexico(ActionEvent event) {
        Lexico("d");
    }

    @FXML
    private void HandleButtonSintatico(ActionEvent event) {
        Sintatico("");
    }

    @FXML
    private void HandleButtonCancelar(ActionEvent event) {
    }

    @FXML
    private void HandleButtonOKConsoleEntered(ActionEvent event) {
        if (!txbComandoConsole.getText().trim().isEmpty()) {
            txbConsole.appendText("> " + txbComandoConsole.getText() + "\n");
            String[] Parametros = txbComandoConsole.getText().split(" ");
            String Parametro = "";
            if (Parametros.length > 1) {
                Parametro = Parametros[1];
            }
            if (Parametros[0].equals("lexico")) {
                Lexico(Parametro);
            }
            if (Parametros[0].equals("sintatico")) {
                Sintatico(Parametro);
            }
            if (Parametros[0].equals("cls")) {
                txbConsole.clear();
            }
            if (Parametros[0].equals("cmd")) {
                Comandos();
            }
            txbComandoConsole.clear();
        }
    }

    public void Lexico(String Parametro) {
        txbConsole.appendText("> " + Analise_Lexica.RealizaAnalise(txbCodigo.getText(), Parametro) + "\n");
        cor.SetTextColor(Analise_Lexica.getAtl().getNTransicao());
        AtualizaTabelaTokens();
    }

    public void Sintatico(String Parametro) {
        Lexico("e");
        String[] Tokens = Analise_Lexica.getAtl().getTokens();
        txbConsole.appendText("> " + Analise_Sintatica.Analise(Tokens) + "\n");
        if (Analise_Sintatica.getTokensComErro().size() > 0) {
            int Menor = Integer.MAX_VALUE, Maior = 0, Linha;
            for (Integer integer : Analise_Sintatica.getTokensComErro()) {
                Linha = Analise_Lexica.getAtl().getLinha(Tokens[integer]);
                if (Linha < Menor) {
                    Menor = Linha;
                }
                if (Linha > Maior) {
                    Maior = Linha;
                }
                txbConsole.appendText("> Linha: " + Linha
                        + "Token Com Erro: " + Tokens[integer] + "\n");
            }
            cor.SetTextColor(Menor, Maior);
        } else {
            cor.Refresh();
        }

    }

    @FXML
    private void HandleCodeKeyChanged(KeyEvent event) {
        cor.Inicializa();
        Analise_Lexica.RealizaAnalise(txbCodigo.getText(), "e");
        String[] Tokens = Analise_Lexica.getAtl().getTokens();
        NodeTransicao = Analise_Lexica.getAtl().getNTransicao();
        Analise_Sintatica.Analise(Tokens);
        TabeladeSimbulos ts = new TabeladeSimbulos();
        ts.add(NodeTransicao);
        for (Integer integer : Analise_Sintatica.getTokensComErro()) {
            NodeTransicao.get(integer).setEstado("Token Rejeitado");
        }
        cor.SetTextColor(NodeTransicao);
        AtualizaTabelaTokens();
    }

    private void AtualizaTabelaTokens() {
        try {
            TreeItem raiz = new TreeItem("Tokens");
            for (NodeTransicao nodeTransicao : Analise_Lexica.getAtl().getNTransicao()) {
                raiz.getChildren().add(nodeTransicao.getTree());
            }
            raiz.setExpanded(true);
            txbTokens.setRoot(raiz);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void HandleButtonSelecionaLinha(MouseEvent event) {
        try {
            NodeTransicao nt = Analise_Lexica.getAtl().getNTransicao()
                    .get(txbTokens.getSelectionModel().getSelectedIndex() - 1);
            selecinaText(nt.getInicio(), nt.getFim() + 1);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void HandleButtonCompilar(ActionEvent event) {
        if (Analise_Sintatica != null
                && Analise_Sintatica.getTokensComErro() != null
                && Analise_Sintatica.getTokensComErro().isEmpty()) {
            CodigoIntermediario cc = new CodigoIntermediario();
            cc.gera(NodeTransicao);
            CodigodeMaquina cm = new CodigodeMaquina();
            cm.gera(NodeTransicao);
            txbConsole.appendText("> Compilado, Arquivos Gerados no diretório principal\n");
        }else{
            txbConsole.appendText("> Codigo Apresenta Erros e não pode ser Compilado\n");
        }
    }

}
