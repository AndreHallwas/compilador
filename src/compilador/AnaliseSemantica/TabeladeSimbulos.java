/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseSemantica;

import compilador.AnaliseLexica.NodeTransicao;
import java.util.ArrayList;
import sun.misc.Queue;

/*
{
    int a = 2;
    a = (2+5)
}
 */
/**
 *
 * @author Aluno
 */
public class TabeladeSimbulos {

    protected ArrayList<NoTabeladeSimbulos> Tabeladesimbulos;
    private int escopo;

    public TabeladeSimbulos() {
        Inicializa();
        escopo = 1;
    }

    private void Inicializa() {
        Tabeladesimbulos = new ArrayList();
    }

    public ArrayList<NodeTransicao> add(ArrayList<NodeTransicao> Tokens) {
        NodeTransicao Token;
        String Tipo, variavel, token;
        parser par = new parser(Tokens);
        escopo = 0;
        while (par.isNotFinal()) {
            par.setCheckPoint();
            Token = par.prox();
            if (Token != null && Token.getToken().equalsIgnoreCase("tk_id")) {
                variavel = Token.getCaracteres();
                token = Token.getToken();
                NoTabeladeSimbulos No = busca_declaracao(variavel, escopo);
                if (No != null) {
                    Token = par.prox();
                    if (Token != null && Token.getToken().equalsIgnoreCase("tk_atribuicao")
                            /*|| Token.getToken().equalsIgnoreCase("tk_id")*/) {
                        Token = par.prox();
                        if (Token != null) {
                            if (Token.getToken().equalsIgnoreCase("tk_caracter")) {
                                if (VerificaTipo(No.getTipo(), Token)) {
                                    String escopoA = No.getEscopo();
                                    Tabeladesimbulos.remove(No);
                                    Tabeladesimbulos.add(new NoTabeladeSimbulos(variavel, Token.getToken(),
                                            "var", No.Tipo, Token.getCaracteres(), escopoA + ""));
                                } else {
                                    System.out.println("Erro de Tipo: " + Token.getCaracteres());
                                    par.setErro();
                                }
                            } else if (Token.getToken().equalsIgnoreCase("tk_abrir_parentese")) {
                                Token = par.prox();
                                if (Token.getToken().equalsIgnoreCase("tk_numero")
                                        || Token.getToken().equalsIgnoreCase("tk_id")) {
                                    if (VerificaTipo(No.getTipo(), Token)) {
                                        String escopoA = No.getEscopo();
                                        Tabeladesimbulos.remove(No);
                                        Tabeladesimbulos.add(new NoTabeladeSimbulos(variavel, Token.getToken(),
                                                "var", No.Tipo, Token.getCaracteres(), escopoA + ""));
                                    } else {
                                        System.out.println("Erro de Tipo: " + Token.getCaracteres());
                                        par.setErro();
                                    }
                                } else if (Token.getToken().equalsIgnoreCase("tk_abrir_parentese")) {
                                    String escopoA = No.getEscopo();
                                    Tabeladesimbulos.remove(No);
                                    Tabeladesimbulos.add(new NoTabeladeSimbulos(variavel, Token.getToken(),
                                            "var", No.Tipo, "expressao", escopoA + ""));
                                }
                            }
                        }
                    } else if (Token.getToken().equalsIgnoreCase("tk_ponto_virgula")) {
                        Tabeladesimbulos.add(new NoTabeladeSimbulos(variavel, token,
                                "var", null, null, escopo + ""));
                    }
                } else {
                    System.out.println("Não Declarado: " + variavel);
                    par.setErro();
                }
            } else if (Token.getToken().equalsIgnoreCase("tk_abrir_chave")) {
                escopo++;
            } else if (Token.getToken().equalsIgnoreCase("tk_fechar_chave")) {
                escopo--;
            } else if (Token.getToken().equalsIgnoreCase("tk_cientifico")
                    || Token.getToken().equalsIgnoreCase("tk_inteiro")
                    || Token.getToken().equalsIgnoreCase("tk_letra")) {
                Tipo = Token.getToken();
                Token = par.prox();
                if (Token != null && Token.getToken().equalsIgnoreCase("tk_id")) {
                    variavel = Token.getCaracteres();
                    token = Token.getToken();
                    NoTabeladeSimbulos No = busca_declaracao(variavel, escopo);
                    if (No != null && No.getEscopo().equals(escopo + "")) {
                        System.out.println("Ja Declarado: " + variavel);
                        par.setErro();
                    } else {
                        Token = par.prox();
                        if (Token != null && Token.getToken().equalsIgnoreCase("tk_atribuicao")) {
                            Token = par.prox();
                            if (Token != null && Token.getToken().equalsIgnoreCase("tk_abrir_parentese")) {
                                Token = par.prox();
                                if (Token != null && Token.getToken().equalsIgnoreCase("tk_numero")
                                        || Token.getToken().equalsIgnoreCase("tk_id")
                                        || Token.getToken().equalsIgnoreCase("tk_letra")) {
                                    Tabeladesimbulos.add(new NoTabeladeSimbulos(variavel, Token.getToken(),
                                            "var", Tipo, Token.getCaracteres(), escopo + ""));
                                }
                            }
                        } else if (Token.getToken().equalsIgnoreCase("tk_ponto_virgula")) {
                            Tabeladesimbulos.add(new NoTabeladeSimbulos(variavel, token,
                                    "var", Tipo, null, escopo + ""));
                        }
                    }
                }
            }
        }
        return par.getArray();

    }

    public NoTabeladeSimbulos busca(NodeTransicao Token) {
        return null;
    }

    /**
     * @return the Tabeladesimbulos
     */
    public ArrayList<NoTabeladeSimbulos> getTabeladesimbulos() {
        return Tabeladesimbulos;
    }

    /**
     * @param Tabeladesimbulos the Tabeladesimbulos to set
     */
    public void setTabeladesimbulos(ArrayList<NoTabeladeSimbulos> Tabeladesimbulos) {
        this.Tabeladesimbulos = Tabeladesimbulos;
    }

    private NoTabeladeSimbulos busca_declaracao(String caracteres, int escopo) {
        Queue fila = new Queue();
        int i = 0;
        while (i < Tabeladesimbulos.size()
                && !(Tabeladesimbulos.get(i).getCadeia().equalsIgnoreCase(caracteres)
                && Tabeladesimbulos.get(i).getEscopo().equalsIgnoreCase(escopo + ""))) {
            if (Tabeladesimbulos.get(i).getCadeia().equalsIgnoreCase(caracteres)
                    && !Tabeladesimbulos.get(i).getEscopo().equalsIgnoreCase(escopo + "")) {
                fila.enqueue(Tabeladesimbulos.get(i));
            }
            i++;
        }
        if (i < Tabeladesimbulos.size()) {
            return Tabeladesimbulos.get(i);
        } else if (!fila.isEmpty()) {
            try {
                NoTabeladeSimbulos aux = (NoTabeladeSimbulos) fila.dequeue();
                NoTabeladeSimbulos auxW;
                if (fila.isEmpty() && Integer.parseInt(aux.Escopo) < escopo) {
                    return aux;
                } else {
                    while (!fila.isEmpty()) {
                        auxW = (NoTabeladeSimbulos) fila.dequeue();
                        if (Integer.parseInt(auxW.Escopo) < escopo
                                && Integer.parseInt(auxW.Escopo) > Integer.parseInt(aux.Escopo)) {
                            aux = auxW;
                        }
                    }
                    if (Integer.parseInt(aux.Escopo) < escopo) {
                        return aux;
                    }
                }
            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
        }
        return null;
    }

    private boolean VerificaTipo(String tipo, NodeTransicao token) {
        if (tipo.equalsIgnoreCase("tk_inteiro")) {
            if (token.getToken().equalsIgnoreCase("tk_numero")) {
                return true;
            } else if (token.getToken().equalsIgnoreCase("tk_id")) {
                NoTabeladeSimbulos nt = busca_declaracao(token.getCaracteres(), escopo);
                if (nt != null) {
                    return VerificaTipoSimples(tipo, nt.Token);
                }
            }
        } else if (tipo.equalsIgnoreCase("tk_letra")) {
            if (token.getToken().equalsIgnoreCase("tk_caracter")) {
                return true;
            } else if (token.getToken().equalsIgnoreCase("tk_id")) {
                NoTabeladeSimbulos nt = busca_declaracao(token.getCaracteres(), escopo);
                if (nt != null) {
                    return VerificaTipoSimples(tipo, nt.Token);
                }
            }

        } else if (tipo.equalsIgnoreCase("tk_cientifico")) {
            if (token.getToken().equalsIgnoreCase("tk_numero")) {
                return true;
            } else if (token.getToken().equalsIgnoreCase("tk_id")) {
                NoTabeladeSimbulos nt = busca_declaracao(token.getCaracteres(), escopo);
                if (nt != null) {
                    return VerificaTipoSimples(tipo, nt.Token);
                }
            }
        }
        return false;
    }

    private boolean VerificaTipoSimples(String tipo, String token) {
        if (tipo.equalsIgnoreCase("tk_inteiro")) {
            return token.equalsIgnoreCase("tk_numero");
        } else if (tipo.equalsIgnoreCase("tk_letra")) {
            return token.equalsIgnoreCase("tk_caracter");
        } else if (tipo.equalsIgnoreCase("tk_cientifico")) {
            return token.equalsIgnoreCase("tk_numero");
        }
        return false;
    }
}
