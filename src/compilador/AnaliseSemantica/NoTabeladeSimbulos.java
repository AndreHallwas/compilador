/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseSemantica;

/**
 *
 * @author Aluno
 */
public class NoTabeladeSimbulos {
    protected String cadeia;
    protected String Token;
    protected String Categoria;
    protected String Tipo;
    protected String Valor;
    protected String Escopo;

    public NoTabeladeSimbulos() {
    }

    public NoTabeladeSimbulos(String cadeia, String Token, String Categoria,
            String Tipo, String Valor, String Escopo) {
        this.cadeia = cadeia;
        this.Token = Token;
        this.Categoria = Categoria;
        this.Tipo = Tipo;
        this.Valor = Valor;
        this.Escopo = Escopo;
    }

    /**
     * @return the cadeia
     */
    public String getCadeia() {
        return cadeia;
    }

    /**
     * @param cadeia the cadeia to set
     */
    public void setCadeia(String cadeia) {
        this.cadeia = cadeia;
    }

    /**
     * @return the Token
     */
    public String getToken() {
        return Token;
    }

    /**
     * @param Token the Token to set
     */
    public void setToken(String Token) {
        this.Token = Token;
    }

    /**
     * @return the Categoria
     */
    public String getCategoria() {
        return Categoria;
    }

    /**
     * @param Categoria the Categoria to set
     */
    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }

    /**
     * @return the Tipo
     */
    public String getTipo() {
        return Tipo;
    }

    /**
     * @param Tipo the Tipo to set
     */
    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    /**
     * @return the Valor
     */
    public String getValor() {
        return Valor;
    }

    /**
     * @param Valor the Valor to set
     */
    public void setValor(String Valor) {
        this.Valor = Valor;
    }

    /**
     * @return the Escopo
     */
    public String getEscopo() {
        return Escopo;
    }

    /**
     * @param Escopo the Escopo to set
     */
    public void setEscopo(String Escopo) {
        this.Escopo = Escopo;
    }
}
