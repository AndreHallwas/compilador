/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compilador.AnaliseSemantica;

import compilador.AnaliseLexica.NodeTransicao;
import java.util.ArrayList;
import sun.misc.Queue;

/**
 *
 * @author Raizen
 */
public class parser {

    private ArrayList<NodeTransicao> Tokens;
    private Queue lista;
    int Pos = 0;

    public parser(ArrayList<NodeTransicao> Tokens) {
        this.Tokens = new ArrayList();
        this.Tokens.addAll(Tokens);
    }

    public NodeTransicao prox() {
        if(lista == null){
             lista = new Queue();
        }
        if (Pos < Tokens.size()) {
            lista.enqueue(Pos);
            return Tokens.get(Pos++);
        }
        return null;
    }
    public boolean isNotFinal(){
        return Pos < Tokens.size();
    }

    public ArrayList<NodeTransicao> getArray() {
        return Tokens;
    }

    public void setCheckPoint() {
         lista = new Queue();
    }

    public void setErro() {
        if(lista != null && !lista.isEmpty()){
            while(!lista.isEmpty()){
                try {
                    Tokens.get((int) lista.dequeue()).setEstado("false");
                } catch (InterruptedException ex) {
                    System.out.println(ex);
                }
            }
        }
    }

}
